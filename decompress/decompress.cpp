#include <cstdio>
#include <cstdlib>
#include <string_view>
#include <cstdio>
#include <numbers>
#include "../sccompr/compressor.h"
#include "../sccompr/data_streams.h"


void invalid_use_exit()
{
	fprintf(stderr, "Invalid use. Use \"-h\" for help.\n");
	std::exit(-1);
}

std::size_t file_size(const char* filename)
{
	FILE* file = fopen(filename, "rb");
	fseek(file, 0, SEEK_END);
	unsigned long long cb = _ftelli64(file);
	verify(cb >= 0);
	fclose(file);
	return static_cast<std::size_t>(cb);
}

void decompress_file(const char* input_file, const char* output_file)
{
	using namespace std;
	auto cb = file_size(input_file);
	InputBitStream ibs = InputBitStream(FileInput<>::create(input_file));
	OutputWordStream ows = OutputWordStream(FileOutput<>::create(output_file));
	decompress_1d_scalars(ibs, cb, ows);
}

int main(int argc, char** argv)
{
	using namespace std::literals::string_view_literals;
	for (int i = 1; i < argc; ++i)
	{
		if (argv[i] == "-input"sv)
		{
			if (++i == argc)
				invalid_use_exit();
			FILE* file = fopen(argv[i], "rb");
		}
		else if (argv[i] == "-output"sv)
		{
			if (++i == argc)
				invalid_use_exit();
			FILE* decompressed_file = fopen(argv[i], "rb");
		}
		else if (argv[i] == "-h"sv)
		{
			fprintf(stdout,
				"decompress -input input_file -output output_file\n"
				"Decompresses an input_file and writes the result to output_file\n");
			std::exit(0);
		}
		else
			invalid_use_exit();
	}
	const char* file = argv[2];
	const char* decompressed_file = argv[4];

	decompress_file(file, decompressed_file);

	printf("Done.\n");
	return 0;
}