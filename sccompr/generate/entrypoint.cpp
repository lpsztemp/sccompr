#include <cstdio>
#include <cstdlib>
#include <string_view>
#include <cstdio>
#include <numbers>
#include <cmath>
#include "../sccompr/config.h"
#include "../sccompr/data_streams.h"

void generate_sine(const char* filename, std::size_t cw, double periods, double magnitude)
{
	OutputWordStream os = OutputWordStream(FileOutput<>::create(filename));
	double dx = std::numbers::pi_v<double> * 2.0 * periods / cw;
	double x = 0.0;
	OutputMemoryWordBuffer owb = os.reserve(cw);
	auto ptr = owb.get_word_ptr();
	for (std::size_t i = 0; i < cw; ++i, x += dx)
		ptr[i] = std::sin(x) * magnitude;
	owb.set_word_length(cw);
	os.commit(std::move(owb));
}
void generate_linear(const char* filename, std::size_t cw, double min_val, double max_val)
{
	OutputWordStream os = OutputWordStream(FileOutput<>::create(filename));
	double dx = (max_val - min_val) / cw;
	double x = min_val;
	OutputMemoryWordBuffer owb = os.reserve(cw);
	auto ptr = owb.get_word_ptr();
	for (std::size_t i = 0; i < cw; ++i, x += dx)
		ptr[i] = x;
	owb.set_word_length(cw);
	os.commit(std::move(owb));
}

void generate_cosine_2D(const char* filename, std::size_t width, std::size_t height, double amplitude, double atteniation, double periods_per_width)
{
	OutputWordStream os = OutputWordStream(FileOutput<>::create(filename));
	auto x_origin = width / 2.0, y_origin = height / 2.0;
	auto dist = [x_origin, y_origin](std::size_t x, std::size_t y)
	{
		double d_x = x - x_origin, d_y = y - y_origin;
		return std::sqrt(d_x * d_x + d_y * d_y);
	};
	auto s = [&dist, x_origin, y_origin, width, amplitude, atteniation, periods_per_width](std::size_t x, std::size_t y)
	{
		double d = dist(x, y);
		return amplitude * std::exp(-d * atteniation) * std::cos(periods_per_width * d / width * 6.283);
	};
	std::size_t cw = width * height;
	OutputMemoryWordBuffer owb = os.reserve(cw);
	auto ptr = owb.get_word_ptr();
	for (std::size_t y = 0; y < height; ++y)
		for (std::size_t x = 0; x < width; ++x)
			ptr[y * width + x] = s(x, y);
	owb.set_word_length(cw);
	os.commit(std::move(owb));
}

enum {LINEAR_SIGNAL, SINE_SIGNAL, COSINE2D_SIGNAL};

struct linear_params
{
	std::size_t cw;
	double min_val, max_val;
};

struct sine_params
{
	std::size_t cw;
	double periods;
	double magnitude;
};

struct cosine2d_params
{
	std::size_t width, height;
	double amplitude, attenuation, periods_per_width;
};

void invalid_use_exit()
{
	fprintf(stderr, "Invalid use. Use \"-h\" for help.\n");
	std::exit(-1);
}

int main(int argc, char** argv)
{
	int method_id;
	linear_params lp;
	sine_params sp;
	cosine2d_params c2dp;
	const char* output_file = nullptr;
	bool id_specified = false;
	using namespace std::literals::string_view_literals;
	for (int i = 1; i < argc; ++i)
	{
		if (argv[i] == "-I"sv)
		{
			if (++i == argc)
				invalid_use_exit();
			if (id_specified)
				invalid_use_exit();
			if (argv[i] == "sin"sv)
			{
				bool magnitude_specified = false, periods_specified = false, size_specified = false;
				method_id = SINE_SIGNAL;
				while (!magnitude_specified || !periods_specified || !size_specified)
				{
					if (++i == argc)
						invalid_use_exit();
					if (argv[i] == "-N"sv)
					{
						if (size_specified || ++i == argc)
							invalid_use_exit();
						long my_cw = std::strtol(argv[i], nullptr, 10);
						if (my_cw <= 0)
							invalid_use_exit();
						sp.cw = (std::size_t) my_cw;
						size_specified = true;
					}else if (argv[i] == "-T"sv)
					{
						if (periods_specified || ++i == argc)
							invalid_use_exit();
						double periods = std::strtod(argv[i], nullptr);
						if (periods <= 0)
							invalid_use_exit();
						sp.periods = periods;
						periods_specified = true;
					}else if (argv[i] == "-M"sv)
					{
						if (magnitude_specified || ++i == argc)
							invalid_use_exit();
						double magnitude = std::strtod(argv[i], nullptr);
						if (magnitude <= 0)
							invalid_use_exit();
						sp.magnitude = magnitude;
						magnitude_specified = true;
					}else
						invalid_use_exit();
				}
			}else if (argv[i] == "lin"sv)
			{
				bool min_specified = false, max_specified = false, size_specified = false;
				method_id = LINEAR_SIGNAL;
				while (!min_specified || !max_specified || !size_specified)
				{
					if (++i == argc)
						invalid_use_exit();
					if (argv[i] == "-N"sv)
					{
						if (size_specified || ++i == argc)
							invalid_use_exit();
						long my_cw = std::strtol(argv[i], nullptr, 10);
						if (my_cw <= 0)
							invalid_use_exit();
						lp.cw = (std::size_t) my_cw;
						size_specified = true;
					}else if (argv[i] == "-min"sv)
					{
						if (min_specified || ++i == argc)
							invalid_use_exit();
						double min_val = std::strtod(argv[i], nullptr);
						lp.min_val = min_val;
						min_specified = true;
					}else if (argv[i] == "-max"sv)
					{
						if (max_specified || ++i == argc)
							invalid_use_exit();
						double max_val = std::strtod(argv[i], nullptr);
						lp.max_val = max_val;
						max_specified = true;
					}else
						invalid_use_exit();
				}
			}else if (argv[i] == "c2d"sv)
			{
				bool width_specified = false, height_specified = false, magnitude_specified = false, attenuation_specified = false, periods_specified = false;
				method_id = COSINE2D_SIGNAL;
				while (!width_specified || !height_specified || !magnitude_specified || !attenuation_specified || !periods_specified)
				{
					if (++i == argc)
						invalid_use_exit();
					if (argv[i] == "-W"sv)
					{
						if (width_specified || ++i == argc)
							invalid_use_exit();
						long my_cx = std::strtol(argv[i], nullptr, 10);
						if (my_cx <= 0)
							invalid_use_exit();
						c2dp.width = (std::size_t) my_cx;
						width_specified = true;
					}else if (argv[i] == "-H"sv)
					{
						if (height_specified || ++i == argc)
							invalid_use_exit();
						long my_cy = std::strtol(argv[i], nullptr, 10);
						if (my_cy <= 0)
							invalid_use_exit();
						c2dp.height = (std::size_t) my_cy;
						height_specified = true;
					}else if (argv[i] == "-M"sv)
					{
						if (magnitude_specified || ++i == argc)
							invalid_use_exit();
						double magnitude = std::strtod(argv[i], nullptr);
						if (magnitude <= 0)
							invalid_use_exit();
						c2dp.amplitude = magnitude;
						magnitude_specified = true;
					}else if (argv[i] == "-A"sv)
					{
						if (attenuation_specified || ++i == argc)
							invalid_use_exit();
						double att = std::strtod(argv[i], nullptr);
						if (att < 0)
							invalid_use_exit();
						c2dp.attenuation = att;
						attenuation_specified = true;
					}else if (argv[i] == "-P"sv)
					{
						if (periods_specified || ++i == argc)
							invalid_use_exit();
						double p = std::strtod(argv[i], nullptr);
						if (p <= 0)
							invalid_use_exit();
						c2dp.periods_per_width = p;
						periods_specified = true;
					}else
						invalid_use_exit();
				}
			}else
				invalid_use_exit();
		}else if (argv[i] == "-h"sv)
		{
			fprintf(stdout,
				"generate -I (sin sine_params|lin line_params|c2d c2d_params) output_file\n"
				"\n"
				"sin         Generate one-dimensional sine signal with the following parameters:\n"
				"  -N size   a number of binary_64 floating point words to generate;\n"
				"  -T n      a number of sine periods per size words of generated data;\n"
				"  -M x      a magnitude of the generated signal.\n"
				"lin         Generate a one-dimensional signal linearly changing in magnitude given its two values.\n"
				"  -N size   a number of binary_64 floating point words to generate;\n"
				"  -min y1   the first value of the genrated signal;\n"
				"  -max y2   the second value of the genrated signal.\n"
				"c2d         Generate a two-dimensional matrix with values of cosine signal originating in the middle of the matrix.\n"
				"  -W cx     with of the matrix, in binary_64 words;\n"
				"  -H cy     height of the matrix;\n"
				"  -M x      magnitude of the generated signal at origin point;\n"
				"  -A a      attenuation of the magnitude with distance (such that distance between two adjacent binary_64 words of the matrix have\n"
				"            a unit measure);\n"
				"  -P p      a number of periods generated per the distance equal to width cx of the matrix."
			);
			std::exit(0);
		}
		else if (output_file != nullptr)
			invalid_use_exit();
		else
			output_file = argv[i];
	}
	if (!output_file)
		invalid_use_exit();
	switch (method_id)
	{
	case LINEAR_SIGNAL:
		generate_linear(output_file, lp.cw, lp.min_val, lp.max_val);
		break;
	case SINE_SIGNAL:
		generate_sine(output_file, sp.cw, sp.periods, sp.magnitude);
		break;
	case COSINE2D_SIGNAL:
		generate_cosine_2D(output_file, c2dp.width, c2dp.height, c2dp.amplitude, c2dp.attenuation, c2dp.periods_per_width);
		break;
	}
	printf("Done.\n");
	return 0;
}