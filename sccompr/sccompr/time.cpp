#include <chrono>
#include "time.h"

class timer: public timestamp
{
	virtual unsigned get_ms() override
	{
		return static_cast<unsigned>(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - tm).count());
	}
	virtual unsigned get_and_rest_ms()
	{
		auto now = std::chrono::steady_clock::now();
		auto ret = now - tm;
		tm = now;
		return static_cast<unsigned>(std::chrono::duration_cast<std::chrono::milliseconds>(ret).count());
	}
private:
	std::chrono::steady_clock::time_point tm = std::chrono::steady_clock::now();
};

timestamp& set_timer()
{
	thread_local timer tm;
	return tm;
}
