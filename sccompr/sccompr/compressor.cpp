#include "compressor.h"
#include "predictor.h"
#include <bit>
#include "bit_helpers.h"

template <class Predictor>
static void compress_1d_scalars(InputWordStream& is, std::size_t input_word_count,  OutputBitStream& os)
{
	Predictor predictor;
	while (input_word_count > 0)
	{
		InputMemoryWordBuffer ib = is.update_buf(input_word_count);
		for (std::size_t i = 0; i < ib.get_word_count(); ++i)
		{
			std::uint64_t pred = predictor.predict_compress(ib.get_word_ptr()[i]);
			std::uint64_t lzc = std::countl_zero(pred | 1) & ~3ull, tnc = 64u - lzc; //trailing non-zero count
			os.write_bits(lzc >> 2, 4);
			os.write_bits(lsb(pred, tnc), tnc);
		}
		input_word_count -= ib.get_word_count();
	}
	//padding
	os.write_bits(1, 1);
	if (os.unaligned())
		os.write_bits(0, 8 - os.unaligned());
}

template <class Predictor>
static void decompress_1d_scalars(InputBitStream& is, std::size_t input_byte_count, OutputWordStream& os)
{
	Predictor predictor;
	std::size_t input_bit_count = input_byte_count * 8;
	OutputMemoryWordBuffer owb = os.reserve(input_byte_count / sizeof(double));
	std::size_t output_size = 0, output_capacity = owb.get_word_cap();
	while (input_bit_count > 8u)
	{
		std::uint64_t lzc = is.read_bits(4) << 2, tnc = 64u - lzc;
		std::uint64_t val = is.read_bits(static_cast<unsigned>(tnc));
		double fval = predictor.predict_decompress(val);
		owb.get_word_ptr()[output_size] = fval;
		if (++output_size == output_capacity)
		{
			os.commit(std::move(owb));
			owb = os.reserve(input_byte_count / sizeof(double));
			output_size = 0;
		}
		input_bit_count -= tnc + 4;
	}
	std::uint8_t last_byte = static_cast<std::uint8_t>(is.read_bits(static_cast<unsigned>(input_bit_count)));
	std::size_t trailing_zeroes = std::countl_zero(last_byte) - (8u - static_cast<int>(input_bit_count));
	input_bit_count -= trailing_zeroes + 1;
	if (input_bit_count != 0)
	{
		verify(input_bit_count >= 4);
		std::uint64_t lzc = lsb(last_byte, 4u) << 2, tnc = 64u - lzc;
		verify(input_bit_count - 4 == tnc);
		last_byte >>= 4;
		double fval = predictor.predict_decompress(lsb(last_byte, tnc));
		owb.get_word_ptr()[output_size++] = fval;
	}
	owb.set_word_length(output_size);
}

template <class Predictor>
void compress_1d_scalars_specialbit(InputWordStream& is, std::size_t input_word_count, OutputBitStream& os)
{
	Predictor predictor;
	while (input_word_count > 0)
	{
		InputMemoryWordBuffer ib = is.update_buf(input_word_count);
		for (std::size_t i = 0; i < ib.get_word_count(); ++i)
		{
			std::uint64_t pred = predictor.predict_compress(ib.get_word_ptr()[i]);
			std::uint64_t lzc = std::countl_zero(pred) & ~3ull, tnc = 64u - lzc; //trailing non-zero count
			if (!lzc)
			{
				os.write_1();
				os.write_bits(pred, 64);
			}else
			{
				os.write_0();
				os.write_bits(lzc >> 2, 4);
				os.write_bits(lsb(pred, tnc), tnc);
			}
		}
		input_word_count -= ib.get_word_count();
	}
	//padding
	os.write_bits(1, 1);
	if (os.unaligned())
		os.write_bits(0, 8 - os.unaligned());
}

template <class Predictor>
void decompress_1d_scalars_specialbit(InputBitStream& is, std::size_t input_byte_count, OutputWordStream& os)
{
	Predictor predictor;
	std::size_t input_bit_count = input_byte_count * 8;
	OutputMemoryWordBuffer owb = os.reserve(input_byte_count / sizeof(double));
	std::size_t output_size = 0, output_capacity = owb.get_word_cap();
	while (input_bit_count > 8u)
	{
		double fval;
		if (is.read_bits(1))
		{
			fval = predictor.predict_decompress(is.read_bits(64));
			input_bit_count -= 65u;
		}else
		{
			std::uint64_t lzc = is.read_bits(4) << 2;
			std::uint64_t val;
			if (!lzc)
			{
				val = 0;
				input_bit_count -= 5;
			}else
			{
				std::size_t tnc = 64u - lzc;
				val = is.read_bits(static_cast<unsigned>(tnc));
				input_bit_count -= tnc + 5;
			}
			fval = predictor.predict_decompress(val);
		}
		owb.get_word_ptr()[output_size] = fval;
		if (++output_size == output_capacity)
		{
			os.commit(std::move(owb));
			owb = os.reserve(input_byte_count / sizeof(double));
			output_size = 0;
		}
	}
	std::uint8_t last_byte = static_cast<std::uint8_t>(is.read_bits(static_cast<unsigned>(input_bit_count)));
	std::size_t trailing_zeroes = std::countl_zero(last_byte) - (8u - static_cast<int>(input_bit_count));
	input_bit_count -= trailing_zeroes + 1;
	if (input_bit_count != 0)
	{
		verify(input_bit_count >= 5);
		verify((last_byte & 1) == 0);
		std::uint64_t lzc = lsb(last_byte, 5u) << 1, tnc = 64u - lzc;
		verify(input_bit_count - 5 == tnc);
		last_byte >>= 5;
		double fval = predictor.predict_decompress(lsb(last_byte, tnc));
		owb.get_word_ptr()[output_size++] = fval;
	}
	owb.set_word_length(output_size);
}

void compress_1d_scalars_v1(InputWordStream& is, std::size_t input_word_count,  OutputBitStream& os)
{
	compress_1d_scalars<PredictorOld>(is, input_word_count, os);
}

void decompress_1d_scalars_v1(InputBitStream& is, std::size_t input_byte_count, OutputWordStream& os)
{
	decompress_1d_scalars<PredictorOld>(is, input_byte_count, os);
}

void compress_1d_scalars_v2(InputWordStream& is, std::size_t input_word_count,  OutputBitStream& os)
{
	compress_1d_scalars_specialbit<PredictorOld>(is, input_word_count, os);
}

void decompress_1d_scalars_v2(InputBitStream& is, std::size_t input_byte_count, OutputWordStream& os)
{
	decompress_1d_scalars_specialbit<PredictorOld>(is, input_byte_count, os);
}

void compress_1d_scalars_v3(InputWordStream& is, std::size_t input_word_count,  OutputBitStream& os)
{
	compress_1d_scalars<Predictor2>(is, input_word_count, os);
}

void decompress_1d_scalars_v3(InputBitStream& is, std::size_t input_byte_count, OutputWordStream& os)
{
	decompress_1d_scalars<Predictor2>(is, input_byte_count, os);
}

void compress_1d_scalars_v4(InputWordStream& is, std::size_t input_word_count,  OutputBitStream& os)
{
	compress_1d_scalars_specialbit<Predictor2>(is, input_word_count, os);
}

void decompress_1d_scalars_v4(InputBitStream& is, std::size_t input_byte_count, OutputWordStream& os)
{
	decompress_1d_scalars_specialbit<Predictor2>(is, input_byte_count, os);
}

//template <class Predictor>
//void compress_1d_vectors(InputWordStream& is, std::size_t input_vector_count,  OutputBitStream& os) //{double, double, double}
//{
//	Predictor predictor[3];
//	std::size_t input_word_count = input_vector_count * 3;
//	std::size_t word_no = 0;
//	while (input_word_count > 0)
//	{
//		InputMemoryWordBuffer ib = is.update_buf(input_word_count);
//		for (std::size_t i = 0; i < ib.get_word_count(); ++i)
//		{
//			std::uint64_t pred = predictor[word_no++ % 3].predict_compress(ib.get_word_ptr()[i]);
//			std::uint64_t lzc = std::countl_zero(pred | 1) & ~3ull, tnc = 64u - lzc; //trailing non-zero count
//			os.write_bits(lzc >> 2, 4);
//			os.write_bits(lsb(pred, tnc), tnc);
//		}
//		input_word_count -= ib.get_word_count();
//	}
//	//padding
//	os.write_bits(1, 1);
//	if (os.unaligned())
//		os.write_bits(0, 8 - os.unaligned());
//}
//
//void decompress_1d_vectors(InputBitStream& is, std::size_t input_byte_count, OutputWordStream& os)
//{
//	Predictor predictor[3];
//	std::size_t word_no = 0;
//	std::size_t input_bit_count = input_byte_count * 8;
//	while (input_bit_count > 8u)
//	{
//		std::uint64_t lzc = is.read_bits(4) << 2, tnc = 64u - lzc;
//		std::uint64_t val = is.read_bits(static_cast<unsigned>(tnc));
//		double fval = predictor[word_no++ % 3].predict_decompress(val);
//		os.push_word(fval);
//		input_bit_count -= tnc + 4;
//	}
//	std::uint8_t last_byte = static_cast<std::uint8_t>(is.read_bits(static_cast<unsigned>(input_bit_count)));
//	std::size_t trailing_zeroes = std::countl_zero(last_byte) - (8u - static_cast<int>(input_bit_count));
//	input_bit_count -= trailing_zeroes + 1;
//	if (input_bit_count != 0)
//	{
//		verify(input_bit_count >= 4);
//		std::uint64_t lzc = lsb(last_byte, 4u) << 2, tnc = 64u - lzc;
//		verify(input_bit_count - 4 == tnc);
//		last_byte >>= 4;
//		double fval = predictor[word_no].predict_decompress(lsb(last_byte, tnc));
//		os.push_word(fval);
//	}
//}
