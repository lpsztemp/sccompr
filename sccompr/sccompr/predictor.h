#pragma once
#include <vector>
#include <cstdint>
#include <cstdlib>
#include <algorithm>
#include "config.h"

class PredictorOld
{
private:
	std::uint64_t last_datum = 0u;
public:
	std::uint64_t predict_compress(double datum);
	double predict_decompress(std::uint64_t datum);
	inline void reset_predictor()
	{
		last_datum = 0u;
	}
};

class Predictor2
{
private:
	static constexpr unsigned order = 3u;
	static constexpr unsigned hash_input_bits = 20u;
	static constexpr unsigned hash_output_bits = 20u;
	static constexpr unsigned prediction_closure_bits = 14u;
private:
	struct hash_value
	{
		std::uint64_t dpred1, dpred2;
	};

	std::vector<hash_value> hash_table = std::vector<hash_value>(std::size_t(1) << hash_input_bits, hash_value{0, 0});
	alignas(16) std::uint64_t deltas[order] = {0, 0, 0}; //deltas[0] is the oldest. alignas(16) is for SSE MOVAPD
	std::uint64_t last_datum = 0u;
private:
	std::uint32_t hash() const;
	std::uint64_t prediction_function(const hash_value& hashval) const;
public:
	std::uint64_t predict_compress(double datum);
	double predict_decompress(std::uint64_t datum);
	inline void reset_predictor()
	{
		std::fill(std::begin(hash_table), std::end(hash_table), hash_value{0, 0});
		std::fill(std::begin(deltas), std::end(deltas), 0);
		last_datum = 0u;
	}
};
