#pragma once
#include <climits>
#include <cfloat>
#include <cstdlib>
#include <cstdio>

#ifndef __cplusplus
#error "C++ is required."
#endif
#ifdef _MSC_VER
#if _MSVC_LANG < 202000u
#error "C++20 is required. Compile with /std:c++latest"
#endif
#elif __cplusplus < 202000u
#error "C++20 is required. Compile with --std:c++20"
#endif

#if FLT_RADIX != 2 || DBL_MANT_DIG != 53
#error "IEEE754 binary64 support via double is required"
#endif

#if CHAR_BIT != 8
#error "A byte must be an 8-bit octet"
#endif

#define verify(expr) do {if (!(expr)) {std::fprintf(stderr, "Failure of \"%s\" at %s:%u", #expr, __FILE__, __LINE__); std::abort();}} while (false)
