#include <bit>
#include "predictor.h"
#include "bit_helpers.h"

std::uint64_t PredictorOld::predict_compress(double datum)
{
	std::uint64_t ld = std::bit_cast<std::uint64_t>(datum) ^ last_datum;
	last_datum ^= ld;
	return ld;
}

double PredictorOld::predict_decompress(std::uint64_t datum)
{
	last_datum ^= datum;
	return std::bit_cast<double>(last_datum);
}
