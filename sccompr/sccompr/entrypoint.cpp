#include <cstdio>
#include <cstdlib>
#include <cstring>
#include "time.h"

const char help_string[] = "profile [-h|<command>]\n\n" \
"EXAMPLES:\n" \
"  profile \"compress.exe -a stateless_predictor_1 -input sine_200pi -output sine_200pi.compr\"\n" \
"  profile \"decompress.exe -a stateless_predictor_1 -input sine_200pi.compr -output sine_200pi.decompr\"\n"
"  profile \"7z.exe a sine_200pi.7z sine_200pi\"\n"
"  profile \"zip.exe sine_200pi.zip sine_200pi\"\n"
"  profile \"rar.exe a sine_200pi.rar sine_200pi\"";

int main(int argc, char** argv)
{
	if (argc < 2 || argc > 3)
	{
		std::fprintf(stderr, "Invalid use; call with \'-h\' for help");
		return -1;
	}else if(!std::strcmp(argv[1],"-h"))
	{
		std::printf(help_string);
		return 0;
	}
	else
	{
		auto& timer = set_timer();
		if (std::system(argv[1]) != 0)
			return -1;
		std::printf("Measured time: %u ms.\n", timer.get_ms());
		return 0;
	}
}