#pragma once
#include <concepts>
#include "config.h"

template <std::integral T, std::unsigned_integral N>
constexpr T lsb(T val, N n)
{
	return n < sizeof(T) * 8?val & (T(1) << n) - 1:val;
}

template <std::integral T, std::unsigned_integral N>
constexpr T msb(T val, N n)
{
	return n < sizeof(T) * 8?val >> (sizeof(T) * 8 - n):val;
}

template <std::integral T, std::integral U>
constexpr auto ceil_div(T dividend, U divisor)
{
	return (dividend + divisor - 1) / divisor;
}