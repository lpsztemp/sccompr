#include <bit>
#include "predictor.h"
#include "bit_helpers.h"

std::uint32_t Predictor2::hash() const
{
	return static_cast<std::uint32_t>(lsb(msb(deltas[2], 14u) ^ (msb(deltas[1], 14u) << 5) ^ msb(deltas[0], 14u) << 10, 20u));
}

std::uint64_t Predictor2::prediction_function(const hash_value& hashval) const
{
	if (std::countl_zero(hashval.dpred1 ^ hashval.dpred2) >= sizeof(std::uint64_t) * 8 - prediction_closure_bits)
		return hashval.dpred1 + hashval.dpred1 - hashval.dpred2;
	else
		return hashval.dpred1;
}

std::uint64_t Predictor2::predict_compress(double datum)
{
	std::uint32_t hk = hash();
	hash_value& hv = hash_table[hk];
	std::uint64_t delta_pred = prediction_function(hv);
	std::uint64_t value_pred = last_datum ^ delta_pred;
	std::uint64_t value_true = std::bit_cast<std::uint64_t>(datum);
	std::uint64_t delta_true = value_true ^ value_pred;
	hv.dpred2 = hv.dpred1;
	deltas[0] = deltas[1];
	deltas[1] = deltas[2];
	last_datum = value_true;
	return hv.dpred1 = deltas[2] = delta_true;
}

double Predictor2::predict_decompress(std::uint64_t delta_true)
{
	std::uint32_t hk = hash();
	hash_value& hv = hash_table[hk];
	std::uint64_t delta_pred = prediction_function(hv);
	std::uint64_t value_pred = last_datum ^ delta_pred;
	std::uint64_t value_true = delta_true ^ value_pred;
	hv.dpred2 = hv.dpred1;
	deltas[0] = deltas[1];
	deltas[1] = deltas[2];
	hv.dpred1 = deltas[2] = delta_true;
	last_datum = value_true;
	return std::bit_cast<double>(value_true);
}
