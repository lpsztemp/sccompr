#include "data_streams.h"
#include <stdexcept>

InputMemoryWordBuffer InputWordStream::update_buf(std::size_t cwHint)
{
	InputMemoryByteBuffer bb = m_ibs->update_buf(cwHint * sizeof(double));
	verify(bb.get_byte_count() % sizeof(double) == 0);
	return InputMemoryWordBuffer{reinterpret_cast<const double*>(bb.get_byte_ptr()), bb.get_byte_count() / sizeof(double)};
}

void InputBitStream::update_buf(std::size_t hint)
{
	InputMemoryByteBuffer inp = m_ibs->update_buf(hint);
	my_buf = inp.get_byte_ptr();
	m_bit_off = m_byte_off = 0;
	m_cb = inp.get_byte_count();
	verify(m_cb > 0 || hint == 0);
}

std::uint64_t InputBitStream::read_bits(unsigned bit_count)
{
	verify(bit_count <= 64);
	if (!bit_count)
		return 0;
	union
	{
		std::uint8_t bytes[sizeof(std::uint64_t)];
		std::uint64_t word;
	} result;
	result.word = 0;
	std::size_t bytes_required_total = ceil_div(bit_count, 8);
	std::size_t complete_bytes_required = bit_count / 8;
	std::size_t remaining_bits_required = bit_count % 8;
	if (m_bit_off == 0)
	{
		std::size_t cbOut = 0;
		if (m_cb - m_byte_off < bytes_required_total)
		{
			std::copy_n(my_buf + m_byte_off, m_cb - m_byte_off, result.bytes);
			bytes_required_total -= m_cb - m_byte_off;
			complete_bytes_required -= m_cb - m_byte_off;
			cbOut += m_cb - m_byte_off;
			std::size_t cbRead = 0;
			while (true)
			{
				this->update_buf(bytes_required_total);
				verify(m_cb > 0);
				cbRead += m_cb;
				if (cbRead >= bytes_required_total)
					break;
				else
				{
					std::copy_n(my_buf, m_cb, result.bytes + cbOut);
					cbOut += m_cb;
					complete_bytes_required -= m_cb;
					bytes_required_total -= m_cb;
				}
			}
		}
		std::copy_n(my_buf + m_byte_off, complete_bytes_required, result.bytes + cbOut);
		m_byte_off += complete_bytes_required;
		cbOut += complete_bytes_required;
		if (remaining_bits_required)
		{
			result.bytes[cbOut] = lsb(my_buf[m_byte_off], remaining_bits_required);
			m_bit_off = remaining_bits_required;
		}
		return result.word;
	}
	unsigned in_byte = 0;
	unsigned out_bit_off_lsb = static_cast<unsigned>(m_bit_off), out_bit_off_msb = 8u - out_bit_off_lsb;
	if (out_bit_off_msb > bit_count)
	{
		result.bytes[0] = lsb(msb(my_buf[m_byte_off], out_bit_off_msb), bit_count); 
		m_bit_off += bit_count;
		return result.word;
	}
	result.bytes[in_byte] = msb(my_buf[m_byte_off++], out_bit_off_msb); 
	bit_count -= out_bit_off_msb;
	if (!bit_count)
		m_bit_off = 0;
	else while (true)
	{
		if (m_byte_off == m_cb)
			this->update_buf(ceil_div(bit_count, 8));
		result.bytes[in_byte] |= my_buf[m_byte_off] << out_bit_off_msb;
		if (bit_count <= out_bit_off_lsb)
		{
			m_bit_off = bit_count;
			result.bytes[in_byte] = lsb(result.bytes[in_byte], bit_count + out_bit_off_msb);
			break;
		}
		bit_count -= out_bit_off_lsb;
		result.bytes[++in_byte] = msb(my_buf[m_byte_off], out_bit_off_msb);
		if (bit_count <= out_bit_off_msb)
		{
			m_bit_off = out_bit_off_lsb + bit_count;
			result.bytes[in_byte] = lsb(result.bytes[in_byte], bit_count);
			break;
		}
		++m_byte_off;
		bit_count -= out_bit_off_msb;
	}
	return result.word;
}

OutputMemoryByteBuffer& OutputMemoryByteBuffer::operator=(OutputMemoryByteBuffer&& right)
{
	if (this != &right)
	{
		if (m_os && m_os != right.m_os)
			m_os->commit(static_cast<OutputMemoryByteBuffer&&>(*this));
		m_os = right.m_os;
		m_ptr = right.m_ptr;
		m_cb = right.m_cb;
		m_count = right.m_count;
		right.m_os = nullptr;
		/*right.m_ptr = nullptr;
		right.m_cb = right.m_count = 0;*/
	}
	return *this;
}

OutputMemoryByteBuffer MemoryOutput::reserve(std::size_t cbReserve)
{
	/*if (cbReserve > m_cb - m_count)
		throw std::bad_alloc();*/
	OutputMemoryByteBuffer result = make_output_byte_buffer(m_buf + m_count, cbReserve);
	return result;
}

void MemoryOutput::commit(OutputMemoryByteBuffer&& buf)
{
	assert(m_buf + m_count == buf.get_byte_ptr());
	std::size_t cb = buf.get_byte_count();
	m_count += cb;
	if (m_count > m_cb || m_count < cb)
	{
		m_count -= m_cb;
		throw std::bad_alloc();
	}
	this->release_buffer(std::move(buf));
}

void OutputBitStream::write_0()
{
	if (m_bit_off != 0)
	{
		if (++m_bit_off == CHAR_BIT)
		{
			m_bit_off = 0u;
			++m_byte_off;
		}
		return;
	}
	if (m_byte_off == m_obb.get_byte_cap())
	{
		m_obs->commit(std::move(m_obb));
		m_obb = m_obs->reserve(reserve_capacity);
		m_byte_off = 0;
	}
	m_obb.get_byte_ptr()[m_byte_off] = 0;
	m_bit_off = 1;
}

void OutputBitStream::write_1()
{
	if (m_bit_off != 0)
	{
		m_obb.get_byte_ptr()[m_byte_off] |= 1 << m_bit_off;
		if (++m_bit_off == CHAR_BIT)
		{
			m_bit_off = 0u;
			++m_byte_off;
		}
		return;
	}
	if (m_byte_off == m_obb.get_byte_cap())
	{
		m_obs->commit(std::move(m_obb));
		m_obb = m_obs->reserve(reserve_capacity);
		m_byte_off = 0;
	}
	m_obb.get_byte_ptr()[m_byte_off] = 1;
	m_bit_off = 1;
}

void OutputBitStream::write_bits(std::uint64_t bits, std::size_t count_lsb)
{
	if (m_bit_off != 0)
	{
		std::size_t avail = 8u - m_bit_off;
		if (avail >= count_lsb)
		{
			m_obb.get_byte_ptr()[m_byte_off] |= lsb(static_cast<std::uint8_t>(bits), count_lsb) << m_bit_off;
			m_bit_off += count_lsb;
			if (m_bit_off == 8u)
			{
				m_bit_off = 0u;
				++m_byte_off;
			}
			return;
		}
		m_obb.get_byte_ptr()[m_byte_off++] |= lsb(static_cast<std::uint8_t>(bits), avail) << m_bit_off;
		bits >>= avail;
		count_lsb -= avail;
		m_bit_off = 0;
	}
	std::size_t full_byte_count = count_lsb / 8;
	std::size_t total_byte_count = ceil_div(count_lsb, 8);
	std::size_t remainder_bits = count_lsb % 8;
	if (remainder_bits)
		bits &= ~UINT64_C(0) >> (64 - count_lsb);
	std::size_t cb_avail = m_obb.get_byte_cap() - m_byte_off;
	if (cb_avail >= total_byte_count)
	{
		std::memcpy(&m_obb.get_byte_ptr()[m_byte_off], &bits, total_byte_count);
		m_byte_off += full_byte_count;
		m_bit_off = remainder_bits;
	}else
	{
		std::memcpy(&m_obb.get_byte_ptr()[m_byte_off], &bits, cb_avail);
		bits >>= cb_avail * CHAR_BIT;
		m_obs->commit(std::move(m_obb));
		m_obb = m_obs->reserve(reserve_capacity);
		std::memcpy(m_obb.get_byte_ptr(), &bits, total_byte_count - cb_avail);
		m_byte_off = full_byte_count - cb_avail;
		m_bit_off = remainder_bits;
	}
}
