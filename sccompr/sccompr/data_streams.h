#pragma once
#include "config.h"
#include <cstdlib>
#include <cstdint>
#include <memory>
#include <cassert>
#include <cstdio>

class InputByteStream;
class InputWordStream;

class InputMemoryByteBuffer
{
	friend class InputByteStream;
	const std::uint8_t* m_ptr = nullptr;
	std::size_t m_cb = 0;
protected:
	//InputMemoryByteBuffer() = default;
	InputMemoryByteBuffer(const std::uint8_t* ptr, std::size_t cb):m_ptr(ptr), m_cb(cb) {}
public:

	inline const std::uint8_t* get_byte_ptr() const
	{
		return m_ptr;
	}
	inline std::size_t get_byte_count() const
	{
		return m_cb;
	}
};

class InputMemoryWordBuffer : public InputMemoryByteBuffer
{
	friend class InputWordStream;
protected:
	//InputMemoryWordBuffer() = default;
	InputMemoryWordBuffer(const double* ptr, std::size_t cw):InputMemoryByteBuffer(reinterpret_cast<const std::uint8_t*>(ptr), cw * sizeof(double)) {}
public:

	inline const double* get_word_ptr() const
	{
		return reinterpret_cast<const double*>(this->get_byte_ptr());
	}
	inline std::size_t get_word_count() const
	{
		return this->get_byte_count() / sizeof(double);
	}
};

class InputByteStream
{
protected:
	inline InputMemoryByteBuffer make_input_byte_buffer(const std::uint8_t* ptr, std::size_t cb)
	{
		return InputMemoryByteBuffer(ptr, cb);
	}
public:
	virtual InputMemoryByteBuffer update_buf(std::size_t cbHint) = 0;
	virtual ~InputByteStream() {}
};

class InputWordStream
{
	std::unique_ptr<InputByteStream> m_ibs;
protected:
	inline InputMemoryWordBuffer make_input_word_buffer(const double* ptr, std::size_t count)
	{
		return InputMemoryWordBuffer(ptr, count);
	}
public:
	//InputWordStream() = default;
	inline InputWordStream(std::unique_ptr<InputByteStream>&& ibs):m_ibs(std::move(ibs)) {}

	InputMemoryWordBuffer update_buf(std::size_t cwHint);
};

#include "bit_helpers.h"

class InputBitStream
{
	std::unique_ptr<InputByteStream> m_ibs;
	const std::uint8_t* my_buf = nullptr;
	std::size_t m_bit_off = 0, m_byte_off = 0, m_cb = 0;
	void update_buf(std::size_t hint);
public:
	inline InputBitStream(std::unique_ptr<InputByteStream>&& ibs):m_ibs(std::move(ibs)) {}
	std::uint64_t read_bits(unsigned bit_count);
};

class MemoryInput : public InputByteStream
{
	const std::uint8_t* m_ptr = nullptr;
	std::size_t m_cbAvail = 0;
	MemoryInput(const void* ptr, std::size_t cb):m_ptr((const std::uint8_t*) ptr), m_cbAvail(cb) {}
public:
	inline static std::unique_ptr<MemoryInput> create(const void* ptr, std::size_t cb)
	{
		return std::unique_ptr<MemoryInput>{new MemoryInput(ptr, cb)};
	}
	inline InputMemoryByteBuffer update_buf(std::size_t cb) override
	{
		if (cb > m_cbAvail)
			cb = m_cbAvail;
		auto result = make_input_byte_buffer(m_ptr, cb);
		m_cbAvail -= cb;
		m_ptr += cb;
		return result;
	}
};

#ifdef _MSC_VER
#pragma warning (disable:4996)
#endif

template <std::size_t MemoryBufferSize = 0x1000>
class FileInput:public InputByteStream
{
	FILE* m_inp = nullptr;
	union sbo_t
	{
		std::uint8_t in_stack[MemoryBufferSize];
		std::uint8_t* in_heap;
	} m_sbo;
	std::size_t m_cbBufAcquired = 0, m_cbBuffered = 0, m_cbBufCap = MemoryBufferSize, m_cbFileAvail;
	inline FileInput(const char* filename):m_inp(std::fopen(filename, "rb"))
	{
		verify(m_inp);
		verify(fseek(m_inp, 0, SEEK_END) == 0);
#ifdef _WIN32
		unsigned long long cb = _ftelli64(m_inp);
#else
		long cb = ftell(m_inp);
#endif
		verify(cb <= SIZE_MAX);
		m_cbFileAvail = (std::size_t) cb;
		verify(fseek(m_inp, 0, SEEK_SET) == 0);
	}
public:
	~FileInput()
	{
		if (m_inp)
			std::fclose(m_inp);
		if (m_cbBufCap > MemoryBufferSize)
			delete [] m_sbo.in_heap;
	}
	inline static std::unique_ptr<FileInput> create(const char* filename)
	{
		return std::unique_ptr<FileInput>{new FileInput(filename)};
	}
	inline InputMemoryByteBuffer update_buf(std::size_t cb) override
	{
		std::size_t buf_avail = m_cbBuffered - m_cbBufAcquired;
		std::uint8_t* data_ptr = m_cbBufCap <= MemoryBufferSize?m_sbo.in_stack:m_sbo.in_heap;
		if (cb <= buf_avail)
		{
			auto result = make_input_byte_buffer(&data_ptr[m_cbBufAcquired], cb);
			m_cbBufAcquired += cb;
			return result;
		}else
		{
			std::size_t avail_total = buf_avail + m_cbFileAvail;
			if (cb <= avail_total)
				cb = avail_total;
			if (cb <= m_cbBufCap)
				std::memmove(data_ptr, data_ptr + m_cbBufAcquired, buf_avail);
			else
			{
				std::uint8_t* new_ptr = new std::uint8_t[cb];
				std::memcpy(new_ptr, data_ptr + m_cbBufAcquired, buf_avail);
				m_sbo.in_heap = new_ptr;
				if (m_cbBufCap > MemoryBufferSize)
					delete [] data_ptr;
				m_cbBufCap = cb;
				data_ptr = new_ptr;

			}
			std::size_t cbRead = std::fread(data_ptr + buf_avail, 1, m_cbBufCap - buf_avail, m_inp);
			verify(cbRead > 0 || !std::ferror(m_inp));
			auto result = make_input_byte_buffer(data_ptr, cb);
			m_cbFileAvail -= cbRead;
			m_cbBuffered = buf_avail + cbRead;
			m_cbBufAcquired = cb;
			return result;
		}
	}
	inline std::size_t available_bytes() const noexcept
	{
		return m_cbBuffered - m_cbBufAcquired + m_cbFileAvail;
	}
};

class OutputByteStream;

class OutputMemoryByteBuffer
{
	friend class OutputByteStream;
	OutputByteStream* m_os = nullptr;
	std::uint8_t* m_ptr;// = nullptr;
	std::size_t m_cb;// = 0;
	std::size_t m_count;// = 0;
protected:
	inline OutputMemoryByteBuffer(OutputByteStream* stream, std::uint8_t* ptr, std::size_t cb):m_os(stream), m_ptr(ptr), m_cb(cb), m_count(cb) {}
public:
	OutputMemoryByteBuffer() = default;
	inline ~OutputMemoryByteBuffer();
	inline OutputMemoryByteBuffer(OutputMemoryByteBuffer&& right)
	{
		*this = std::move(right);
	}
	OutputMemoryByteBuffer& operator=(OutputMemoryByteBuffer&& right);
	
	inline std::uint8_t* get_byte_ptr() const
	{
		return m_ptr;
	}
	inline std::size_t get_byte_cap() const
	{
		return m_cb;
	}
	inline std::size_t get_byte_count() const
	{
		return m_count;
	}
	inline void set_byte_length(std::size_t count)
	{
		verify(count <= m_cb);
		m_count = count;
	}
};

class OutputByteStream
{
	friend class OutputMemoryByteBuffer;
protected:
	inline OutputMemoryByteBuffer make_output_byte_buffer(std::uint8_t* ptr, std::size_t cb)
	{
		return OutputMemoryByteBuffer(this, ptr, cb);
	}
	inline void release_buffer(OutputMemoryByteBuffer&& buf)
	{
		buf.m_os = nullptr;
	}
public:
	virtual OutputMemoryByteBuffer reserve(std::size_t cbReserve) = 0;
	virtual void commit(OutputMemoryByteBuffer&& buf) = 0;
	virtual ~OutputByteStream() {}
};

OutputMemoryByteBuffer::~OutputMemoryByteBuffer()
{
	if (m_os)
		m_os->commit(static_cast<OutputMemoryByteBuffer&&>(*this));
}

class MemoryOutput:public OutputByteStream
{
	std::uint8_t* m_buf = nullptr;
	std::size_t m_count = 0, m_cb = 0;
	MemoryOutput(void* ptr, std::size_t cb):m_buf(static_cast<std::uint8_t*>(ptr)), m_cb(cb) {}
public:
	inline static std::unique_ptr<MemoryOutput> create(void* ptr, std::size_t cb)
	{
		return std::unique_ptr<MemoryOutput>{new MemoryOutput(ptr, cb)};
	}
	virtual OutputMemoryByteBuffer reserve(std::size_t cbReserve) override;
	virtual void commit(OutputMemoryByteBuffer&& buf) override;
	inline std::size_t count() const
	{
		return m_count;
	}
};

#include <list>

template <std::size_t Alignment = 1, std::size_t SmallBufferOptimization = 0x1000>
class AccumulatingMemoryOutput:public OutputByteStream
{
	class node
	{
		std::size_t m_cap = 0u, m_size = 0u;
		union datum_sbo_t
		{
			alignas(Alignment) std::uint8_t sbo[SmallBufferOptimization];
			std::uint8_t* heap;
		} m_datum;
	public:
		node() = default;
		inline explicit node(std::size_t count):m_cap(SmallBufferOptimization)
		{
			if (count > sbo_size)
			{
				m_datum.heap = static_cast<std::uint8_t*>(::operator new[](count, std::align_val_t(Alignment)));
				m_cap = count;
			}
		}
		inline ~node()
		{
			if (m_cap > sbo_size)
				::operator delete[](m_datum.heap, m_cap, std::align_val_t(Alignment));
		}
		inline std::uint8_t* ptr()
		{
			return m_cap <= sbo_size?m_datum.sbo:m_datum.heap;
		}
		inline const std::uint8_t* ptr() const
		{
			return m_cap <= sbo_size?m_datum.sbo:m_datum.heap;
		}
		std::uint8_t* reserve(std::size_t count)
		{
			verify(count <= this->capacity());
			auto result = this->ptr() + m_size;
			return result;
		}
		void set_size(std::size_t cb)
		{
			verify(cb <= m_cap);
			m_size = cb;
		}
		std::size_t size() const
		{
			return m_size;
		}
		std::size_t capacity() const
		{
			return m_cap - m_size;
		}
	};
	std::list<node> m_lst;
	std::size_t byte_count_total = 0;
	constexpr static std::size_t sbo_size = SmallBufferOptimization;
public:
	inline static std::unique_ptr<AccumulatingMemoryOutput<Alignment, sbo_size>> create()
	{
		return std::unique_ptr<AccumulatingMemoryOutput<Alignment, sbo_size>>{new AccumulatingMemoryOutput<Alignment, sbo_size>()};
	}
	virtual OutputMemoryByteBuffer reserve(std::size_t cbReserve) override
	{
		if (!m_lst.empty())
		{
			if (m_lst.back().capacity() >= cbReserve)
			{
				OutputMemoryByteBuffer result = make_output_byte_buffer(m_lst.back().reserve(cbReserve), cbReserve);
				return result;
			}
		}
		m_lst.emplace_back(cbReserve);
		return make_output_byte_buffer(m_lst.back().ptr(), cbReserve);
	}
	virtual void commit(OutputMemoryByteBuffer&& buf) override
	{
		assert(m_lst.back().ptr() + m_lst.back().size() == buf.get_byte_ptr());
		std::size_t cb = buf.get_byte_count();
		if (cb != 0)
		{
			node& nd = m_lst.back();
			nd.set_size(nd.size() + cb);
		}
		this->release_buffer(std::move(buf));
		byte_count_total += cb;
	}
	inline std::size_t count() const
	{
		return byte_count_total;
	}
	std::size_t read_accumulated(void* dest, std::size_t cap) const
	{
		auto pb = static_cast<std::uint8_t*>(dest);
		verify(cap >= byte_count_total);
		for (const node& nd:m_lst)
		{
			std::copy_n(nd.ptr(), nd.size(), pb);
			pb += nd.size();
		}
		return byte_count_total;
	}
	void clear()
	{
		m_lst.clear();
		byte_count_total = 0;
	}
};

template <std::size_t MemoryBufferSize = 0x1000>
class FileOutput:public OutputByteStream
{
	FILE* m_out = nullptr;
	union sbo_t
	{
		std::uint8_t in_stack[MemoryBufferSize];
		std::uint8_t* in_heap;
	} m_sbo;
	std::size_t m_cbAvail = MemoryBufferSize;
	FileOutput(const char* filename):m_out(std::fopen(filename, "wb")) {}
public:
	~FileOutput()
	{
		if (m_cbAvail > MemoryBufferSize)
			delete [] m_sbo.in_heap;
		if (m_out)
			fclose(m_out);
	}
	inline static std::unique_ptr<FileOutput> create(const char* filename)
	{
		return std::unique_ptr<FileOutput>{new FileOutput(filename)};
	}
	virtual OutputMemoryByteBuffer reserve(std::size_t cbReserve) override
	{
		if (cbReserve <= MemoryBufferSize)
			return make_output_byte_buffer(m_sbo.in_stack, cbReserve);
		else if (m_cbAvail < cbReserve)
		{
			if (m_cbAvail > MemoryBufferSize)
				delete [] m_sbo.in_heap;
			m_sbo.in_heap = new std::uint8_t[cbReserve];
			m_cbAvail = cbReserve;
		}
		return make_output_byte_buffer(m_sbo.in_heap, cbReserve);
	}
	virtual void commit(OutputMemoryByteBuffer&& buf) override
	{
		std::size_t cb = buf.get_byte_count();
		auto data = buf.get_byte_cap() <= MemoryBufferSize? m_sbo.in_stack :m_sbo.in_heap;
		assert(data == buf.get_byte_ptr());
		verify(std::fwrite(data, 1, cb, m_out) == cb);
		this->release_buffer(std::move(buf));
		std::fflush(m_out);
	}
};

class OutputMemoryWordBuffer:public OutputMemoryByteBuffer
{
	friend class OutputWordStream;
protected:
	inline explicit OutputMemoryWordBuffer(OutputMemoryByteBuffer&& bob)
		:OutputMemoryByteBuffer(std::move(bob)) {}
public:
	inline double* get_word_ptr() const
	{
		return reinterpret_cast<double*>(this->get_byte_ptr());
	}
	inline std::size_t get_word_cap() const
	{
		return this->get_byte_cap() / sizeof(double);
	}
	inline std::size_t get_word_count() const
	{
		return this->get_byte_count() / sizeof(double);
	}
	inline void set_word_length(std::size_t cw)
	{
		this->OutputMemoryByteBuffer::set_byte_length(cw * sizeof(double));
	}
};

class OutputWordStream
{
	std::unique_ptr<OutputByteStream> m_obs;
public:
	OutputWordStream(std::unique_ptr<OutputByteStream>&& obs):m_obs(std::move(obs)) {}
	inline void push_word(std::uint64_t wrd)
	{
		auto ob = this->reserve(1);
		assert(ob.get_word_cap() > 0);
		std::memcpy(ob.get_word_ptr(), &wrd, 8u);
		ob.set_word_length(1);
	}
	void push_word(double wrd)
	{
		auto ob = this->reserve(1);
		assert(ob.get_word_cap() > 0);
		*ob.get_word_ptr() = wrd;
		ob.set_word_length(1);
	}
	inline OutputMemoryWordBuffer reserve(std::size_t cw)
	{
		return OutputMemoryWordBuffer(m_obs->reserve(cw * sizeof(double)));
	}
	inline void commit(OutputMemoryWordBuffer&& buf)
	{
		m_obs->commit(std::move(buf));
	}
	template <std::derived_from<OutputByteStream> OutputByteStreamType>
	std::unique_ptr<OutputByteStreamType> release_byte_stream_as() &&
	{
		return std::unique_ptr<OutputByteStreamType>(dynamic_cast<OutputByteStreamType*>(m_obs.release()));
	}
	inline std::unique_ptr<OutputByteStream> release_byte_stream() &&
	{
		return std::move(m_obs);
	}
};

class OutputBitStream
{
	std::unique_ptr<OutputByteStream> m_obs;
	OutputMemoryByteBuffer m_obb;
	static constexpr std::size_t reserve_capacity = 0x1000;
	std::size_t m_bit_off = 0, m_byte_off = 0;
public:
	inline OutputBitStream(std::unique_ptr<OutputByteStream>&& obs):m_obs(std::move(obs)), m_obb(m_obs->reserve(reserve_capacity)) {}
	inline ~OutputBitStream()
	{
		if (m_obs)
		{
			m_obb.set_byte_length(m_byte_off);
			m_obs->commit(std::move(m_obb));
		}
	}
	void write_0();
	void write_1();
	void write_bits(std::uint64_t bits, std::size_t count_lsb);
	inline void flush()
	{
		assert(m_bit_off == 0);
		m_obb.set_byte_length(m_byte_off);
		m_obs->commit(std::move(m_obb));
		m_obb = m_obs->reserve(reserve_capacity);
		m_bit_off = m_byte_off = 0;
	}
	template <std::derived_from<OutputByteStream> OutputByteStreamType>
	std::unique_ptr<OutputByteStreamType> release_byte_stream_as() &&
	{
		assert(m_bit_off == 0);
		m_obb.set_byte_length(m_byte_off);
		m_obs->commit(std::move(m_obb));
		return std::unique_ptr<OutputByteStreamType>(dynamic_cast<OutputByteStreamType*>(m_obs.release()));
	}
	inline std::unique_ptr<OutputByteStream> release_byte_stream() &&
	{
		return std::move(*this).release_byte_stream_as<OutputByteStream>();
	}
	inline std::size_t unaligned() const
	{
		return m_bit_off % 8;
	}
};
