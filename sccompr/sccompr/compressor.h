#pragma once
#include "data_streams.h"
#include "predictor.h"

//Old predictor
void compress_1d_scalars_v1(InputWordStream& is, std::size_t input_word_count,  OutputBitStream& os);
void decompress_1d_scalars_v1(InputBitStream& is, std::size_t input_byte_count, OutputWordStream& os);

void compress_1d_scalars_v2(InputWordStream& is, std::size_t input_word_count,  OutputBitStream& os);
void decompress_1d_scalars_v2(InputBitStream& is, std::size_t input_byte_count, OutputWordStream& os);

//New predictor
void compress_1d_scalars_v3(InputWordStream& is, std::size_t input_word_count,  OutputBitStream& os);
void decompress_1d_scalars_v3(InputBitStream& is, std::size_t input_byte_count, OutputWordStream& os);

void compress_1d_scalars_v4(InputWordStream& is, std::size_t input_word_count,  OutputBitStream& os);
void decompress_1d_scalars_v4(InputBitStream& is, std::size_t input_byte_count, OutputWordStream& os);
