#include <cstdio>
#include <cstdlib>
#include <string_view>
#include <cstdio>
#include <numbers>
#include <functional>
#include <concepts>
#include "../sccompr/compressor.h"
#include "../sccompr/data_streams.h"


void invalid_use_exit()
{
	fprintf(stderr, "Invalid use. Use \"-h\" for help.\n");
	std::exit(-1);
}

template <std::invocable<InputWordStream&, std::size_t, OutputBitStream&> CompressFn>
void compress_file(CompressFn compress_fn, const char* input_file, const char* output_file)
{
	using namespace std;
	auto file_input = FileInput<>::create(input_file);
	auto cb = file_input->available_bytes();
	verify(cb % sizeof(double) == 0);
	InputWordStream iws = InputWordStream(std::move(file_input));
	OutputBitStream obs = OutputBitStream(FileOutput<>::create(output_file));
	compress_fn(iws, cb / sizeof(double), obs);
}


int main(int argc, char** argv)
{
	using namespace std::literals::string_view_literals;
	const char *input_file = nullptr, *output_file = nullptr;
	std::function<void(InputWordStream&, std::size_t, OutputBitStream&)> compress_fn;
	for (int i = 1; i < argc; ++i)
	{
		if (argv[i] == "-input"sv)
		{
			if (++i == argc || !!input_file)
				invalid_use_exit();
			input_file = argv[i];
		}else if (argv[i] == "-output"sv)
		{
			if (++i == argc || !!output_file)
				invalid_use_exit();
			output_file = argv[i];
		}else if (argv[i] == "-h"sv)
		{
			fprintf(stdout,
				"compress -a alg -input input_file -output output_file\n"
				"Compresses an input_file and writes the result to outpit_file.\n"
				"\n"
				"alg identifies the compression algorithm used. The following values of alg are supported:\n"
				"   stateless_predictor_1 - a compressor based on the stateless predictor PredictorOld.\n"
				"   stateless_predictor_2 - same but with special processing of diff values with no leading\n"
				"                           zeroes.\n"
				"   hash_predictor_1      - a compressor using the hash-table based predictor Predictor2.\n"
				"   hash_predictor_2      - same but with special processing of diff values with no leading\n"
				"                           zeroes.\n"
			);
			std::exit(0);
		}else if (argv[i] == "-a"sv)
		{
			if (++i == argc || !!compress_fn)
				invalid_use_exit();
			if (argv[i] == "stateless_predictor_1"sv)
				compress_fn = compress_1d_scalars_v1;
			else if (argv[i] == "stateless_predictor_2"sv)
				compress_fn = compress_1d_scalars_v2;
			else if (argv[i] == "hash_predictor_1"sv)
				compress_fn = compress_1d_scalars_v3;
			else if (argv[i] == "hash_predictor_2"sv)
				compress_fn = compress_1d_scalars_v4;
			else
				invalid_use_exit();
		}else
			invalid_use_exit();
	}
	if (!input_file || !output_file || !compress_fn)
		invalid_use_exit();

	compress_file(compress_fn, input_file, output_file);

	printf("Done.\n");
	return 0;
}