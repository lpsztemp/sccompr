#Visualize 2D binary_64 data
import os
import argparse
import matplotlib.pyplot as plt 
import numpy as np
import math

params = argparse.ArgumentParser(description="Visualize a two-dimensional binary_64 signal in a file.")
params.add_argument('datafile', type=argparse.FileType('rb'), help="A file to read data from.");
params.add_argument('-W', '--width', help="Width of the matrix to visualize, in binary_64 words. By default, if the height is specified, the width is the file size, in words, divided by the height. If neither width nor height are specified, both are calculated as a square root of file size (in words). If the size is not a perfect square the script fails.");
params.add_argument('-H', '--height', help="Height of the matrix to visualize, in binary_64 words. See above for the description of default behavior.");
params = params.parse_args()

H = W = 0
N = params.datafile.seek(0, os.SEEK_END) / 8
params.datafile.seek(0, os.SEEK_SET)
if params.width == None:
	if params.height == None:
		W = int(math.floor(math.sqrt(N)))
		if W * W != N:
			raise ValueError("Width")
		H = W
	else:
		H = int(params.height, 0)
		if N % H != 0:
			raise ValueError("Height")
		W = int(N // H)
else:
	W = int(params.width, 0)
	if params.height == None:
		if N % W != 0:
			raise ValueError("Width")
		H = int(N // W)
	else:
		H = int(params.height, 0)
		

loaded_array = np.fromfile(params.datafile).reshape((W, H))

plot = plt.pcolormesh(loaded_array, cmap='RdBu', shading='flat')
#cset = plt.contour(loaded_array, cmap='gray')
#plt.clabel(cset, inline=True)
plt.colorbar(plot)
plt.show()
