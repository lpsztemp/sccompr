#Visualize 1D binary_64 data
import argparse
import matplotlib.pyplot as plt 
import numpy as np

params = argparse.ArgumentParser(description="Visualize a one-dimensional binary_64 signal in a file.")
params.add_argument('datafile', type=argparse.FileType('rb'), help="A file to read data from.");
params = params.parse_args()

loaded_array = np.fromfile(params.datafile)

plt.plot(loaded_array)
plt.show()