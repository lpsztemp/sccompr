import pandas as pd
import os
import argparse
import datetime
import sys
import shutil
import math
import shlex
import re
from subprocess import Popen, PIPE
import matplotlib.pyplot as plt
import filecmp

default_log_size = 10
default_experiment_count = 1
dir_sep = '\\'
default_zip = 'zip.exe'
default_unzip = 'unzip.exe'
default_7z = '7z.exe'
default_rar = 'rar.exe'
cur_dir = f'{os.getcwd()}{dir_sep}'
tmp_dir = os.environ['TMP']
current_time = datetime.datetime.utcnow()
datetime_string = f'{current_time.year}-{current_time.month}-{current_time.day}_{current_time.hour}-{current_time.minute}-{current_time.second}'
nil = 'NUL'

def get_program_full_name(program_name):
	script_dir=os.path.dirname(os.path.realpath(__file__))
	name = f'{script_dir}\\..\\x64\\Release\\{program_name}.exe'
	if not os.path.isfile(name):
		raise FileNotFoundError(f'{name} does not exist. Build the executable first.')
	return name

compress_executable = get_program_full_name('compress')
decompress_executable = get_program_full_name('decompress')
zip_executable = shutil.which(default_zip)
unzip_executable = shutil.which(default_unzip)
sz_executable = shutil.which(default_7z)
rar_executable = shutil.which(default_rar)

profile_executable = get_program_full_name('profile')
generate_executable = get_program_full_name('generate')

def validate_unique_filename(base_name):
	if not os.path.isfile(base_name):
		return base_name
	n = 0
	new_file_name = f'{base_name}_{n}'
	while os.path.isfile(new_file_name):
		n += 1
		new_file_name = f'{base_name}_{n}'
	return new_file_name

def sanitize_file_name(name):
	return re.sub('[^\w]', '-', name.strip())

def generate_line(count, min_val, max_val):
	file_name = validate_unique_filename(f'{cur_dir}line_{count}_{min_val}_{max_val}_{datetime_string}')
	invocation = f'{generate_executable} -I lin -min {min_val} -max {max_val} -N {count} \"{file_name}\" > {nil}'
	os.system(invocation)
	return file_name

def generate_sine(count, periods, magnitude):
	file_name = validate_unique_filename(f'{cur_dir}sine_{count}_{periods}_{magnitude}_{datetime_string}')
	invocation = f'{generate_executable} -I sin -T {periods} -M {magnitude} -N {count} \"{file_name}\" > {nil}'
	os.system(invocation)
	return file_name

def generate_c2ds(width, height, magnitude, attenuation, periods):
	file_name = validate_unique_filename(f'{cur_dir}c2ds_{width}_{height}_{magnitude}_{attenuation}_{periods}_{datetime_string}')
	invocation = f'{generate_executable} -I c2d -W {width} -H {height} -M {magnitude} -A {attenuation} -P {periods} \"{file_name}\" > {nil}'
	os.system(invocation)
	return file_name

def get_process_output(invocation):
	process = Popen(invocation, stdout=PIPE)
	(output, err) = process.communicate()
	exit_code = process.wait()
	if exit_code != 0:
		raise ChildProcessError(f'The profiler failed with error {exit_code}.')
	return output.decode("utf-8").rstrip()

def profile_run(invocation_string):
	output = get_process_output([profile_executable, invocation_string])
	return int(re.search('Measured time: (\d+) ms.', output).group(1))

def extract_archive(archive_name, output_file, archived_file_name, profile_decompress_to_directory):
	my_tmp_dir = os.path.join(tmp_dir, f'compr_{datetime_string}')
	file = archived_file_name
	time = profile_decompress_to_directory(archive_name, my_tmp_dir)
	shutil.move(os.path.join(my_tmp_dir, file), output_file)
	shutil.rmtree(my_tmp_dir)
	return time

class diploma:
	name = "Diploma"
	algorithm = ""
	def __init__(self, name, algorithm):
		self.name = name
		self.algorithm = algorithm
	def compress(self, input_file, output_file):
		return profile_run(f'\"{compress_executable}\" -a {self.algorithm} -input {input_file} -output {output_file}')
	def decompress(self, input_file, output_file):
		return profile_run(f'\"{decompress_executable}\" -a {self.algorithm} -input {input_file} -output {output_file}')

class zip_compressor:
	name = "ZIP"
	def compress(input_file, output_file):
		return profile_run(f'\"{zip_executable}\" -9 -j {output_file} {input_file}')
	def decompress(input_file, output_file):
		file = get_process_output([unzip_executable, '-Z1', input_file])
		extract_fn = lambda inp, out_dir: profile_run(f'\"{unzip_executable}\" -d {out_dir} {inp}')
		return extract_archive(input_file, output_file, file, extract_fn)

class sz_compressor:
	name = "7Z"
	def compress(input_file, output_file):
		return profile_run(f'\"{sz_executable}\" a -bd -sse -mmt1 {output_file} {input_file}')
	def decompress(input_file, output_file):
		output = get_process_output([sz_executable, 'l', '-ba', input_file])
		file = re.search(' ([^ ]+)$', output).group(1)
		extract_fn = lambda inp, out_dir: profile_run(f'\"{sz_executable}\" e -bd -sse -mmt1 -o{out_dir} {inp}')
		return extract_archive(input_file, output_file, file, extract_fn)

class rar_compressor:
	name = "RAR"
	def compress(input_file, output_file):
		return profile_run(f'\"{rar_executable}\" a -mt1 -ai -isnd- -qo- -s {output_file} {input_file}')
	def decompress(input_file, output_file):
		file = get_process_output([rar_executable, 'lb', input_file])
		extract_fn = lambda inp, out_dir: profile_run(f'\"{rar_executable}\" x -mt1 -ai -isnd- {inp} * {out_dir}\\')
		return extract_archive(input_file, output_file, file, extract_fn)

compressors = [diploma("Diploma, Stateless 1", "stateless_predictor_1"), diploma("Diploma, Stateless 2", "stateless_predictor_2"), diploma("Diploma, Hash 1", "hash_predictor_1"), diploma("Diploma, Hash 2", "hash_predictor_2")]
if zip_executable == None or unzip_executable == None:
	print("Warning: ZIP compressor could not be found")
else:
	compressors.append(zip_compressor)
if sz_executable == None:
	print("Warning: 7z compressor could not be found")
else:
	compressors.append(sz_compressor)
if rar_executable == None:
	print("Warning: RAR compressor could not be found")
else:
	compressors.append(rar_compressor)


def compress_decompress(compressor, input_file, keep_files, check_decompressed):
	try:
		compressed_file = f'{input_file}.{sanitize_file_name(compressor.name)}.compressed'
		decompressed_file = f'{input_file}.{sanitize_file_name(compressor.name)}.decompressed'
		compression_time = compressor.compress(input_file, compressed_file)
		factor = os.path.getsize(input_file) / os.path.getsize(compressed_file)
		decompression_time = compressor.decompress(compressed_file, decompressed_file)
		if check_decompressed and not filecmp.cmp(input_file, decompressed_file, shallow=False):
			raise RuntimeError("Generated and decompressed files are different")
		return [factor, compression_time, decompression_time]
	finally:
		if not keep_files:
			if compressed_file != None and os.path.isfile(compressed_file):
				os.remove(compressed_file)
			if decompressed_file != None and os.path.isfile(decompressed_file):
				os.remove(decompressed_file)
	
def efficiency_size(generator, min_log_size = 6, max_log_size = default_log_size, keep_files = False, check_decompressed = True, **generator_params):
	if min_log_size < 0 or min_log_size > max_log_size:
		raise ValueError("Invalid parameter")
	factors = []
	compression_times = []
	decompression_performance = []
	for i in range(min_log_size, max_log_size + 1):
		N = 2**i
		factors_row = [N * 8]
		compression_times_row = [N * 8]
		decompression_times_row = [N * 8]
		line = [N * 8]
		for c in compressors:
			try:
				generated_file = generator(count=N, **generator_params)
				(factor, compression_time, decompression_time) = compress_decompress(c, generated_file, keep_files, check_decompressed)
				factors_row.append(factor)
				compression_times_row.append(compression_time)
				decompression_times_row.append(decompression_time)
				line += [factor, compression_time, decompression_time]
			finally:
				if not keep_files:
					if generated_file != None and os.path.isfile(generated_file):
						os.remove(generated_file)
		print(line)
		factors.append(factors_row)
		compression_times.append(compression_times_row)
		decompression_performance.append(decompression_times_row)
	df_factors = pd.DataFrame(factors, columns=['Size'] + [c.name for c in compressors]).set_index('Size')
	df_compression_times = pd.DataFrame(compression_times, columns=['Size'] + [c.name for c in compressors]).set_index('Size')
	df_decompression_times = pd.DataFrame(decompression_performance, columns=['Size'] + [c.name for c in compressors]).set_index('Size')
	return {'factors': df_factors, 'compression_times': df_compression_times, 'decompression_times': df_decompression_times}

def line_compression_efficiency_size(compressors = compressors, min_val = 0, max_val = 10, min_log_size = 6, max_log_size = default_log_size, keep_files = False, check_decompressed = True):
	print(f'Line, factors, min_val={min_val}, max_val={max_val}')
	return {'data_type': "Line"} | efficiency_size(generate_line, min_log_size, max_log_size, keep_files, check_decompressed, min_val=min_val, max_val=max_val)

def line_compression_factor_mins(compressors = compressors, size = 2**default_log_size, min_min = -1.1E+6, min_max = 1.1E+6, min_step = None, angle_deg = 5, keep_files = False, check_decompressed = True):
	print(f'Line, factors, N={size}, min_0={min_min}, min_1={min_max}, angle={angle_deg} degrees')
	if size < 1:
		raise ValueError("Invalid parameter")
	if min_step == None:
		min_step = (min_max - min_min) / 10
	measurements = []
	while min_min <= min_max:
		row = [min_min]
		for c in compressors:
			try:
				generated_file = generate_line(count = size, min_val = min_min, max_val = min_min + math.tan(angle_deg * math.pi / 180) * size)
				compressed_file = f'{generated_file}.{sanitize_file_name(c.name)}.compressed'
				c.compress(generated_file, compressed_file)
				factor = os.path.getsize(generated_file) / os.path.getsize(compressed_file)
				if check_decompressed:
					decompressed_file = f'{generated_file}.{sanitize_file_name(c.name)}.decompressed'
					c.decompress(compressed_file, decompressed_file)
					if not filecmp.cmp(generated_file, decompressed_file, shallow=False):
						raise RuntimeError("Generated and decompressed files are different")
				row.append(factor)
			finally:
				if not keep_files:
					if generated_file != None and os.path.isfile(generated_file):
						os.remove(generated_file)
					if compressed_file != None and os.path.isfile(compressed_file):
						os.remove(compressed_file)
					if decompressed_file != None and os.path.isfile(decompressed_file):
						os.remove(decompressed_file)
		print(row)
		measurements.append(row)
		min_min += min_step
	return {'data_type': "Line", 'factors': pd.DataFrame(measurements, columns=['Starting abscissa'] + [c.name for c in compressors]).set_index('Starting abscissa')}

def line_compression_factor_angles(compressors = compressors, size = 2**default_log_size, min_val = 0, angle_deg_min = -180, angle_deg_max = 180, angle_deg_step = 5, keep_files = False, check_decompressed = True):
	print(f'Line, factors, N={size}, min={min_val}, ang_0={angle_deg_min}, ang_1={angle_deg_max}')
	if size < 1:
		raise ValueError("Invalid parameter")
	measurements = []
	while angle_deg_min <= angle_deg_max:
		row = [angle_deg_min]
		for c in compressors:
			try:
				generated_file = generate_line(count = size, min_val = min_val, max_val = min_val + math.tan(angle_deg_min * math.pi / 180) * size)
				compressed_file = f'{generated_file}.{sanitize_file_name(c.name)}.compressed'
				c.compress(generated_file, compressed_file)
				factor = os.path.getsize(generated_file) / os.path.getsize(compressed_file)
				if check_decompressed:
					decompressed_file = f'{generated_file}.{sanitize_file_name(c.name)}.decompressed'
					c.decompress(compressed_file, decompressed_file)
					if not filecmp.cmp(generated_file, decompressed_file, shallow=False):
						raise RuntimeError("Generated and decompressed files are different")
				row.append(factor)
			finally:
				if not keep_files:
					if generated_file != None and os.path.isfile(generated_file):
						os.remove(generated_file)
					if compressed_file != None and os.path.isfile(compressed_file):
						os.remove(compressed_file)
					if decompressed_file != None and os.path.isfile(decompressed_file):
						os.remove(decompressed_file)
		print(row)
		measurements.append(row)
		angle_deg_min += angle_deg_step
	return {'data_type': "Line", 'factors': pd.DataFrame(measurements, columns=['Angle'] + [c.name for c in compressors]).set_index('Angle')}

def sine_compression_efficiency_size(compressors = compressors, periods = 1, magnitude = 1, min_log_size = 6, max_log_size = default_log_size, keep_files = False, check_decompressed = True):
	print(f'Sine, factors, periods={periods}, magnitude={magnitude}')
	return {'data_type': "Sine"} | efficiency_size(generate_sine, min_log_size, max_log_size, keep_files, check_decompressed, periods = periods, magnitude=magnitude)

def sine_compression_factor_periods(compressors = compressors, magnitude = 10, size = 2**default_log_size, periods_min = 1, periods_max = 15, periods_step = 0.5, keep_files = False, check_decompressed = True):
	print(f'Sine, factors, size={size}, magnitude={magnitude}')
	if size < 1 or periods_min <= 0 or periods_min > periods_max:
		raise ValueError("Invalid parameter")
	measurements = []
	while periods_min <= periods_max:
		row = [periods_min]
		for c in compressors:
			try:
				generated_file = generate_sine(count = size, periods=periods_min, magnitude=magnitude)
				compressed_file = f'{generated_file}.{sanitize_file_name(c.name)}.compressed'
				c.compress(generated_file, compressed_file)
				factor = os.path.getsize(generated_file) / os.path.getsize(compressed_file)
				if check_decompressed:
					decompressed_file = f'{generated_file}.{sanitize_file_name(c.name)}.decompressed'
					c.decompress(compressed_file, decompressed_file)
					if not filecmp.cmp(generated_file, decompressed_file, shallow=False):
						raise RuntimeError("Generated and decompressed files are different")
				row.append(factor)
			finally:
				if not keep_files:
					if generated_file != None and os.path.isfile(generated_file):
						os.remove(generated_file)
					if compressed_file != None and os.path.isfile(compressed_file):
						os.remove(compressed_file)
					if decompressed_file != None and os.path.isfile(decompressed_file):
						os.remove(decompressed_file)
		print(row)
		measurements.append(row)
		periods_min += periods_step
	return {'data_type': "Sine", 'factors': pd.DataFrame(measurements, columns=['Periods'] + [c.name for c in compressors]).set_index('Periods')}

def c2d_compression_efficiency_size(compressors = compressors, magnitude = 10, attenuation = 0, periods=10, min_log_size = 6, max_log_size = default_log_size, keep_files = False, check_decompressed = True):
	print(f'C2D, factors, magnitude={magnitude}, attenuation={attenuation}, periods={periods}')
	if min_log_size < 0 or min_log_size > max_log_size:
		raise ValueError("Invalid parameter")
	factors = []
	compression_times = []
	decompression_performance = []
	for i in range(min_log_size, max_log_size + 1, 2):
		N = 2**(i // 2)
		factors_row = [N * 8]
		compression_times_row = [N * 8]
		decompression_times_row = [N * 8]
		line = [N * 8]
		for c in compressors:
			try:
				generated_file = generate_c2ds(width = N, height = N, magnitude = magnitude, attenuation = attenuation, periods = periods)
				(factor, compression_time, decompression_time) = compress_decompress(c, generated_file, keep_files, check_decompressed)
				factors_row.append(factor)
				compression_times_row.append(compression_time)
				decompression_times_row.append(decompression_time)
				line += [factor, compression_time, decompression_time]
			finally:
				if not keep_files:
					if generated_file != None and os.path.isfile(generated_file):
						os.remove(generated_file)
		print(line)
		factors.append(factors_row)
		compression_times.append(compression_times_row)
		decompression_performance.append(decompression_times_row)
	df_factors = pd.DataFrame(factors, columns=['Size'] + [c.name for c in compressors]).set_index('Size')
	df_compression_times = pd.DataFrame(compression_times, columns=['Size'] + [c.name for c in compressors]).set_index('Size')
	df_decompression_times = pd.DataFrame(decompression_performance, columns=['Size'] + [c.name for c in compressors]).set_index('Size')
	return {'data_type': "C2D", 'factors': df_factors, 'compression_times': df_compression_times, 'decompression_times': df_decompression_times}

def export(base_name, data):
	styles = ['-', '--','-.',':','-', '--','-.',':','-', '--','-.',':']
	colors = ['black', 'black', 'black', 'black', '#888', '#888', '#888', '#888', '#aaa', '#aaa', '#aaa', '#aaa']
	name = data['data_type']
	for (nm, datum) in data.items():
		if nm != 'data_type':
			ax = datum.plot(style=styles, color=colors)
			ax.set_xlabel(datum.index.name)
			ax.set_ylabel(f'{name}, {nm}')
			plt.savefig(f'{base_name}_{nm}_{datetime_string}.png', bbox_inches='tight')
			#В Excel: Создаем новый файл, дальше "Данные"->"Из текста"->"Далее"->"Символом разделителем является - запятая"
			#  На третьем шаге, выбирая один за другим все столбцы, нажимать "Подробнее" и указывать символом-разделителем точку.
			datum.to_csv(f'{base_name}_{nm}_{datetime_string}.csv', line_terminator='\n')#, sep=';', decimal=',')

max_log_size = 25

export('line_size', line_compression_efficiency_size(min_val = -100, max_val = 100, min_log_size = 1, max_log_size = max_log_size))
export('line_factors_mins', line_compression_factor_mins(size = 2**max_log_size, min_min = -10, min_max = 10, angle_deg = 5))
export('line_factors_angles', line_compression_factor_angles(size = 2**max_log_size, min_val = -10, angle_deg_min = -90, angle_deg_max = 90, angle_deg_step = 10))
export('sine_size', sine_compression_efficiency_size(periods=10, magnitude=0.5, min_log_size = 1, max_log_size=max_log_size))
export('sine_factors_periods', sine_compression_factor_periods(magnitude=0.5, size = 2**max_log_size, periods_min=1, periods_max=1000, periods_step=100))
export('c2d_factor_size', c2d_compression_efficiency_size(magnitude=10, attenuation=0.03, periods=10, min_log_size=2, max_log_size=max_log_size))
