#include <cstdio>
#include <cstdlib>
#include <string_view>
#include <cstdio>
#include <numbers>
#include <functional>
#include <concepts>
#include "../sccompr/compressor.h"
#include "../sccompr/data_streams.h"


void invalid_use_exit()
{
	fprintf(stderr, "Invalid use. Use \"-h\" for help.\n");
	std::exit(-1);
}

template <std::invocable<InputBitStream&, std::size_t, OutputWordStream&> DecompressFn>
void decompress_file(DecompressFn decompress_fn, const char* input_file, const char* output_file)
{
	using namespace std;
	auto file_input = FileInput<>::create(input_file);
	auto cb = file_input->available_bytes();
	InputBitStream ibs = InputBitStream(std::move(file_input));
	OutputWordStream ows = OutputWordStream(FileOutput<>::create(output_file));
	decompress_fn(ibs, cb, ows);
}

int main(int argc, char** argv)
{
	using namespace std::literals::string_view_literals;
	const char *input_file = nullptr, *output_file = nullptr;
	std::function<void(InputBitStream&, std::size_t, OutputWordStream&)> decompress_fn;
	for (int i = 1; i < argc; ++i)
	{
		if (argv[i] == "-input"sv)
		{
			if (++i == argc || !!input_file)
				invalid_use_exit();
			input_file = argv[i];
		}else if (argv[i] == "-output"sv)
		{
			if (++i == argc || !!output_file)
				invalid_use_exit();
			output_file = argv[i];
		}else if (argv[i] == "-h"sv)
		{
			fprintf(stdout,
				"decompress -a alg -input input_file -output output_file\n"
				"Decompresses an input_file and writes the result to output_file\n"
				"\n"
				"alg identifies the compression algorithm used. The following values of alg are supported:\n"
				"   stateless_predictor_1 - a compressor based on the stateless predictor PredictorOld.\n"
				"   stateless_predictor_2 - same but with special processing of diff values with no leading\n"
				"                           zeroes.\n"
				"   hash_predictor_1      - a compressor using the hash-table based predictor Predictor2.\n"
				"   hash_predictor_2      - same but with special processing of diff values with no leading\n"
				"                           zeroes.\n"
			);
			std::exit(0);
		}else if (argv[i] == "-a"sv)
		{
			if (++i == argc || !!decompress_fn)
				invalid_use_exit();
			if (argv[i] == "stateless_predictor_1"sv)
				decompress_fn = decompress_1d_scalars_v1;
			else if (argv[i] == "stateless_predictor_2"sv)
				decompress_fn = decompress_1d_scalars_v2;
			else if (argv[i] == "hash_predictor_1"sv)
				decompress_fn = decompress_1d_scalars_v3;
			else if (argv[i] == "hash_predictor_2"sv)
				decompress_fn = decompress_1d_scalars_v4;
			else
				invalid_use_exit();
		}else
			invalid_use_exit();
	}
	if (!input_file || !output_file || !decompress_fn)
		invalid_use_exit();

	decompress_file(decompress_fn, input_file, output_file);

	printf("Done.\n");
	return 0;
}