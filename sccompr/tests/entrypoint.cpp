bool all_stream_tests();
bool all_compressor_tests();

#include "../sccompr/compressor.h"
#include "../sccompr/data_streams.h"
#include <memory>
#include <numbers>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <algorithm>

bool sine_compression_dbg()
{
	constexpr std::size_t plain_size = 4, compressed_buf_size = (plain_size + plain_size) * sizeof(double), decompressed_buf_size = plain_size;
	double input[plain_size], decompressed[decompressed_buf_size];
	std::uint8_t compressed[compressed_buf_size];
	double dx = std::numbers::pi_v<double> * 2.0 / plain_size;
	double x = 0.0;
	for (std::size_t i = 0; i < plain_size; ++i)
	{
		input[i] = std::sin(x);
		x += dx;
	}

	auto iws = InputWordStream(MemoryInput::create(input, plain_size * sizeof(double)));
	auto compr_os = OutputBitStream(MemoryOutput::create(compressed, compressed_buf_size * sizeof(double)));
	compress_1d_scalars_v2(iws, plain_size, compr_os);
	auto compr_bs = std::move(compr_os).release_byte_stream_as<MemoryOutput>();
	std::size_t compressed_size = compr_bs->count();
	std::cerr << "Compressed from " << plain_size * sizeof(double) << " to " << compressed_buf_size << " bytes.\n";

	auto ibs = InputBitStream(MemoryInput::create(compressed, compressed_size));
	auto decompr_ws = OutputWordStream(MemoryOutput::create(decompressed, decompressed_buf_size * sizeof(double)));
	decompress_1d_scalars_v2(ibs, compressed_size, decompr_ws);
	auto decompr_bs = std::move(decompr_ws).release_byte_stream_as<MemoryOutput>();
	if (decompr_bs->count() != plain_size * sizeof(double))
		return false;
	auto mm = std::mismatch(input, input + plain_size, decompressed);
	if (mm.first - input != plain_size)
	{
		std::cerr << "Mismatch of 0x" << std::hex << std::setw(16) << std::bit_cast<std::uint64_t>(*mm.first) << " and "
			<< std::setw(16) << std::bit_cast<std::uint64_t>(*mm.second) << " at word offset " << std::dec << mm.first - input << "\n";
		return false;
	}
	return true;
}

int main(int argc, char** argv)
{
	if (!sine_compression_dbg())
		return -1;
	if (!all_stream_tests())
		return -1;
	if (!all_compressor_tests())
		return -1;
	return 0;
}