#include "../sccompr/compressor.h"
#include "../sccompr/data_streams.h"
#include <memory>
#include <numbers>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <concepts>

template <
	std::invocable<InputWordStream&, std::size_t, OutputBitStream&> CompressFn,
	std::invocable<InputBitStream&, std::size_t, OutputWordStream&> DecompressFn>
bool compression_test_base(CompressFn compress_fn, DecompressFn decompress_fn, const double* input, std::size_t dbl_count)
{
	auto iws = InputWordStream(MemoryInput::create(input, dbl_count * sizeof(double)));
	auto compr_os = OutputBitStream(AccumulatingMemoryOutput<>::create());
	compress_fn(iws, dbl_count, compr_os);
	auto compr_bs = std::move(compr_os).release_byte_stream_as<AccumulatingMemoryOutput<>>();
	std::size_t compressed_size = compr_bs->count();
	auto compressed = std::make_unique<std::uint8_t[]>(compressed_size);
	compr_bs->read_accumulated(compressed.get(), compressed_size);
	std::cerr << "Compressed from " << dbl_count * sizeof(double) << " to " << compressed_size << " bytes.\n";
	compr_bs->clear();

	auto ibs = InputBitStream(MemoryInput::create(compressed.get(), compressed_size));
	auto decompr_ws = OutputWordStream(AccumulatingMemoryOutput<>::create());
	decompress_fn(ibs, compressed_size, decompr_ws);
	auto decompr_bs = std::move(decompr_ws).release_byte_stream_as<AccumulatingMemoryOutput<>>();
	if (decompr_bs->count() != dbl_count * sizeof(double))
		return false;
	auto decompressed = std::make_unique<double[]>(dbl_count);
	decompr_bs->read_accumulated(decompressed.get(), dbl_count * sizeof(double));
	auto mm = std::mismatch(input, input + dbl_count, decompressed.get());
	if (mm.first - input != dbl_count)
	{
		std::cerr << "Mismatch of 0x" << std::hex << std::setw(16) << std::bit_cast<std::uint64_t>(*mm.first) << " and "
			<< std::setw(16) << std::bit_cast<std::uint64_t>(*mm.second) << " at word offset " << std::dec << mm.first - input << "\n";
		return false;
	}
	return true;
}

template <class G> requires (std::is_invocable_r_v<double, G, double>)
bool compression_test_generate(G generate, std::size_t dbl_count, double dx)
{
	auto input = std::make_unique<double[]>(dbl_count);
	double x = 0.0;
	for (std::size_t i = 0; i < dbl_count; ++i)
		input[i] = generate(x), x += dx;
	if (!compression_test_base(compress_1d_scalars_v1, decompress_1d_scalars_v1, input.get(), dbl_count))
		return false;
	if (!compression_test_base(compress_1d_scalars_v2, decompress_1d_scalars_v2, input.get(), dbl_count))
		return false;
	if (!compression_test_base(compress_1d_scalars_v3, decompress_1d_scalars_v3, input.get(), dbl_count))
		return false;
	if (!compression_test_base(compress_1d_scalars_v4, decompress_1d_scalars_v4, input.get(), dbl_count))
		return false;
	return true;
}

bool constant_compression(std::size_t dbl_count)
{
	return compression_test_generate([](double) {return 10.0;}, dbl_count, 0.0);
}

bool sine_compression(std::size_t dbl_count)
{
	constexpr std::size_t number_of_periods = 1;
	return compression_test_generate([] (double x) {return std::sin(x);}, dbl_count, std::numbers::pi_v<double> * 2.0 * number_of_periods / dbl_count);
}

template <std::invocable<InputWordStream&, std::size_t, OutputBitStream&> CompressFn>
static void compress_file(CompressFn compress_fn, const char* input_file, const char* output_file)
{
	using namespace std;
	auto file_input = FileInput<>::create(input_file);
	auto cb = file_input->available_bytes();
	verify(cb % sizeof(double) == 0);
	InputWordStream iws = InputWordStream(std::move(file_input));
	OutputBitStream obs = OutputBitStream(FileOutput<>::create(output_file));
	compress_fn(iws, cb / sizeof(double), obs);
}

template <std::invocable<InputBitStream&, std::size_t, OutputWordStream&> DecompressFn>
static void decompress_file(DecompressFn decompress_fn, const char* input_file, const char* output_file)
{
	using namespace std;
	auto file_input = FileInput<>::create(input_file);
	auto cb = file_input->available_bytes();
	InputBitStream ibs = InputBitStream(std::move(file_input));
	OutputWordStream ows = OutputWordStream(FileOutput<>::create(output_file));
	decompress_fn(ibs, cb, ows);
}

static bool compare_files(const char* filename1, const char* filename2)
{
	using namespace std;
	FILE* file1 = fopen(filename1, "rb"), *file2 = fopen(filename2, "rb");
	char buf1[0x100], buf2[0x100];
	std::size_t cb1, cb2;
	verify(file1);
	verify(file2);
	cb1 = fread(buf1, 1, sizeof(buf1), file1);
	verify(cb1 > 0 || !ferror(file1));
	cb2 = fread(buf2, 1, sizeof(buf2), file2);
	verify(cb2 > 0 || !ferror(file2));
	while (cb1 > 0)
	{
		if (cb1 != cb2)
			return false;
		if (std::memcmp(buf1, buf2, cb1) != 0)
			return false;
		cb1 = fread(buf1, 1, sizeof(buf1), file1);
		verify(cb1 > 0 || !ferror(file1));
		cb2 = fread(buf2, 1, sizeof(buf2), file2);
	verify(cb2 > 0 || !ferror(file2));
	}
	return !cb2;
}

template <
	std::invocable<InputWordStream&, std::size_t, OutputBitStream&> CompressFn,
	std::invocable<InputBitStream&, std::size_t, OutputWordStream&> DecompressFn>
static bool compression_test_file_using(CompressFn compress_fn, DecompressFn decompress_fn)
{
	const char data_file[] = "data8MiB";
	const char compressed_file[] = "compressed_data_8MiB";
	const char decompressed_file[] = "decompressed_data_8MiB";
	compress_file(compress_fn, data_file, compressed_file);
	decompress_file(decompress_fn, compressed_file, decompressed_file);
	return compare_files(data_file, decompressed_file);
}

static bool compression_test_file()
{
	if (!compression_test_file_using(compress_1d_scalars_v1, decompress_1d_scalars_v1))
		return false;
	if (!compression_test_file_using(compress_1d_scalars_v2, decompress_1d_scalars_v2))
		return false;
	if (!compression_test_file_using(compress_1d_scalars_v3, decompress_1d_scalars_v3))
		return false;
	if (!compression_test_file_using(compress_1d_scalars_v4, decompress_1d_scalars_v4))
		return false;
	return true;
}

bool all_compressor_tests()
{
	if (!constant_compression(1000))
		return false;
	if (!sine_compression(4))
		return false;
	if (!sine_compression(100))
		return false;
	if (!sine_compression(1000000))
		return false;
	if (!sine_compression(100000000))
		return false;
	if (!compression_test_file())
		return false;
	return true;
}
