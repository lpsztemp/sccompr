#include "../sccompr/data_streams.h"
#include <cstdlib>
#include <ranges>
#include <iostream>

static bool copy_byte_memory_test()
{
	std::uint8_t input_buffer[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
	std::uint8_t output_buffer[sizeof(input_buffer)];
	std::size_t blob_size[] = {0, 1, 2, 3, 4, 5};
	std::size_t iblb = 0;
	auto is = MemoryInput::create(input_buffer, sizeof(input_buffer));
	auto os = MemoryOutput::create(output_buffer, sizeof(output_buffer));
	while (true)
	{
		std::size_t blb = blob_size[iblb++];
		auto in_buf = is->update_buf(blb);
		auto out_buf = os->reserve(in_buf.get_byte_count());
		std::copy_n(in_buf.get_byte_ptr(), in_buf.get_byte_count(), out_buf.get_byte_ptr());
		out_buf.set_byte_length(in_buf.get_byte_count());
		if (in_buf.get_byte_count() < blb)
			break;
	}
	return memcmp(input_buffer, output_buffer, sizeof(input_buffer)) == 0;
}
static bool copy_word_memory_test()
{
	double input_buffer[13];
	double output_buffer[std::size(input_buffer)];
	std::size_t blob_size[12];
	std::size_t iblb = 0;
	std::ranges::copy(std::ranges::iota_view(0, 6), blob_size);
	std::ranges::copy(std::ranges::iota_view(1, 14), input_buffer);
	auto is = InputWordStream(MemoryInput::create(input_buffer, sizeof(input_buffer)));
	auto os = OutputWordStream(MemoryOutput::create(output_buffer, sizeof(output_buffer)));
	while (true)
	{
		std::size_t blb = blob_size[iblb++];
		auto in_buf = is.update_buf(blb);
		auto out_buf = os.reserve(in_buf.get_word_count());
		std::copy_n(in_buf.get_word_ptr(), in_buf.get_word_count(), out_buf.get_word_ptr());
		out_buf.set_word_length(in_buf.get_word_count());
		if (in_buf.get_word_count() < blb)
			break;
	}
	return memcmp(input_buffer, output_buffer, sizeof(input_buffer)) == 0;
}

static bool copy_bit_memory_acc_test_short()
{
	std::size_t blob_size = 3;
	                               // v  v  v    v  v  v       v  v
	const std::uint8_t input_buffer[] = {0B10001000, 0B11000110, 0B11111010, 0B10001000, 0B11000110, 0B11111010};
	std::uint8_t output_buffer[sizeof(input_buffer)];
	std::size_t bits_read = 0;
	auto is = InputBitStream(MemoryInput::create(input_buffer, sizeof(input_buffer)));
	auto os = OutputBitStream(AccumulatingMemoryOutput<1, 4>::create());
	while (bits_read < sizeof(input_buffer) * 8)
	{
		auto bits = is.read_bits(unsigned(blob_size));
		os.write_bits(bits, blob_size);
		bits_read += blob_size;
	}
	auto acc = std::move(os).release_byte_stream_as<AccumulatingMemoryOutput<1, 4>>();
	return acc->read_accumulated(output_buffer, sizeof(output_buffer)) == sizeof(output_buffer) &&
		memcmp(input_buffer, output_buffer, sizeof(input_buffer)) == 0;
}

static bool copy_bit_memory_acc_test_long()
{
	std::size_t blob_size = 61;
	const std::uint8_t input_buffer[61] = {
		0xAA, 0xAB, 0xAC, 0xAD, 0xAE, 0xAF, 0xB0, 0xB1, 0xB2, 0xB3,
		0xB4, 0xB5, 0xB6, 0xB7, 0xB8, 0xB9, 0xBA, 0xBB, 0xBC, 0xBD,
		0xBE, 0xBF, 0xC0, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7,
		0xC8, 0xC9, 0xCA, 0xCB, 0xCC, 0xCD, 0xCE, 0xCF, 0xD0, 0xD1,
		0xD2, 0xD3, 0xD4, 0xD5, 0xD6, 0xD7, 0xD8, 0xD9, 0xDA, 0xDB,
		0xDC, 0xDD, 0xDE, 0xDF, 0xE0, 0xE1, 0xE2, 0xE3, 0xE4, 0xE5,
		0xE6
	};
	const std::uint64_t ctrl_bits[8] = {
		0x11B0AFAEADACABAAull, 0x0DC5BDB5ADA59D95ull, 0x102FEFAF6F2EEEAEull, 0x118F8D8B89878583ull,
		0x0CFCECDCCCBCAC9Cull, 0x0BEB6AEA69E968E8ull, 0x1F7B77736F6B6763ull, 0x1CDCBC9C7C5C3C1Bull,
	};
	std::uint8_t output_buffer[sizeof(input_buffer)];
	auto is = InputBitStream(MemoryInput::create(input_buffer, sizeof(input_buffer)));
	auto os = OutputBitStream(AccumulatingMemoryOutput<1, 4>::create());
	for (std::size_t i = 0; i < std::size(ctrl_bits); ++i)
	{
		auto bits = is.read_bits(unsigned(blob_size));
		if (bits != ctrl_bits[i])
			return false;
		os.write_bits(bits, blob_size);
	}
	auto acc = std::move(os).release_byte_stream_as<AccumulatingMemoryOutput<1, 4>>();
	return acc->read_accumulated(output_buffer, sizeof(output_buffer)) == sizeof(output_buffer) &&
		memcmp(input_buffer, output_buffer, sizeof(input_buffer)) == 0;
}

static bool compare_files(const char* filename1, const char* filename2)
{
	using namespace std;
	FILE* file1 = fopen(filename1, "rb"), *file2 = fopen(filename2, "rb");
	char buf1[0x100], buf2[0x100];
	std::size_t cb1, cb2;
	verify(file1);
	verify(file2);
	cb1 = fread(buf1, 1, sizeof(buf1), file1);
	verify(cb1 > 0 || !ferror(file1));
	cb2 = fread(buf2, 1, sizeof(buf2), file2);
	verify(cb2 > 0 || !ferror(file2));
	while (cb1 > 0)
	{
		if (cb1 != cb2)
			return false;
		if (std::memcmp(buf1, buf2, cb1) != 0)
			return false;
		cb1 = fread(buf1, 1, sizeof(buf1), file1);
		verify(cb1 > 0 || !ferror(file1));
		cb2 = fread(buf2, 1, sizeof(buf2), file2);
	verify(cb2 > 0 || !ferror(file2));
	}
	return !cb2;
}

static bool copy_bit_files_test_long()
{
	std::size_t blob_size = 61;
	const std::uint64_t ctrl_bits[8] = {
		0x11B0AFAEADACABAAull, 0x0DC5BDB5ADA59D95ull, 0x102FEFAF6F2EEEAEull, 0x118F8D8B89878583ull,
		0x0CFCECDCCCBCAC9Cull, 0x0BEB6AEA69E968E8ull, 0x1F7B77736F6B6763ull, 0x1CDCBC9C7C5C3C1Bull,
	};
	auto is = InputBitStream(FileInput<4>::create("testinput"));
	auto os = OutputBitStream(FileOutput<5>::create("testoutput"));
	for (std::size_t i = 0; i < std::size(ctrl_bits); ++i)
	{
		auto bits = is.read_bits(unsigned(blob_size));
		if (bits != ctrl_bits[i])
			return false;
		os.write_bits(bits, blob_size);
	}
	os.flush();
	return compare_files("testinput", "testoutput");
}

static bool copy_bit_memory_acc_test()
{
	if (!copy_bit_memory_acc_test_short())
		return false;
	if (!copy_bit_memory_acc_test_long())
		return false;
	return true;
}

bool all_stream_tests()
{
	if (!copy_byte_memory_test())
	{
		std::cerr << "copy_byte_memory_test failed.\n";
		return false;
	}
	if (!copy_word_memory_test())
	{
		std::cerr << "copy_word_memory_test failed.\n";
		return false;
	}
	if (!copy_bit_memory_acc_test())
	{
		std::cerr << "copy_bit_memory_acc_test failed.\n";
		return false;
	}
	if (!copy_bit_files_test_long())
	{
		std::cerr << "copy_bit_files_test_long failed.\n";
		return false;
	}

	return true;
}
