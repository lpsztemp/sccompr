CLASS_BEGIN(`Element')
CLASS_END

CLASS_BEGIN(`RealScalar')
CLASS_MEMBER(`+value:double')
CLASS_END
NODE_NAME(`RealScalar')->NODE_NAME(`Element') [INHERITANCE]

CLASS_BEGIN(`ComplexScalar')
CLASS_MEMBER(`+real:double')
CLASS_MEMBER(`+imag:double')
CLASS_END
NODE_NAME(`ComplexScalar')->NODE_NAME(`Element') [INHERITANCE]

CLASS_BEGIN(`Vector<ScalarType>')
CLASS_MEMBER(`+value:ScalarType')
CLASS_MEMBER(`+dims:size_t')
CLASS_END
NODE_NAME(`Vector<ScalarType>')->NODE_NAME(`Element') [INHERITANCE]

CLASS_BEGIN(`Matrix<ScalarType>')
CLASS_MEMBER(`+value:ScalarType')
CLASS_MEMBER(`+cols:uint32_t')
CLASS_MEMBER(`+rows:uint32_t')
CLASS_END
NODE_NAME(`Matrix<ScalarType>')->NODE_NAME(`Element') [INHERITANCE]
