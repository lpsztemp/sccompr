@echo off
where /q omcd || ECHO ERROR: Install OMCD. && EXIT /B
where /q dot || ECHO ERROR: Install Graphviz. && EXIT /B
set dir=%~dp0
for %%f in (*.dot) do dot -Tpng %%f -o %%~nf.png
for %%f in (*.txt) do omcd -T png %%f %%~nf.png
