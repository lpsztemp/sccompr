#include <cstdio>
#include <cstdlib>
#include <string_view>
#include <cstdio>
#include <numbers>
#include "../sccompr/compressor.h"
#include "../sccompr/data_streams.h"


void invalid_use_exit()
{
	fprintf(stderr, "Invalid use. Use \"-h\" for help.\n");
	std::exit(-1);
}

std::size_t file_size(const char* filename)
{
	FILE* file = fopen(filename, "rb");
	fseek(file, 0, SEEK_END);
	unsigned long long cb = _ftelli64(file);
	verify(cb >= 0);
	fclose(file);
	return static_cast<std::size_t>(cb);
}

void compress_file(const char* input_file, const char* output_file)
{
	using namespace std;
	auto cb = file_size(input_file);
	verify(cb % sizeof(double) == 0);
	InputWordStream iws = InputWordStream(FileInput<>::create(input_file));
	OutputBitStream obs = OutputBitStream(FileOutput<>::create(output_file));
	compress_1d_scalars(iws, cb / sizeof(double), obs);
}


int main(int argc, char** argv)
{
	using namespace std::literals::string_view_literals;
	for (int i = 1; i < argc; ++i)
	{
		if (argv[i] == "-input"sv)
		{
			if (++i == argc)
				invalid_use_exit();
			FILE* file = fopen(argv[i], "rb");
		}
		else if (argv[i] == "-output"sv)
		{
			if (++i == argc)
				invalid_use_exit();
			FILE* compressed_file = fopen(argv[i], "rb");
		}
		else if (argv[i] == "-h"sv)
		{
			fprintf(stdout,
				"compress -input input_file -output output_file\n"
				"Compresses an input_file and writes the result to outpit_file\n");
		std::exit(0);
		}
		else
			invalid_use_exit();
	}
	const char* file = argv[2];
	const char* compressed_file = argv[4];

	compress_file(file, compressed_file);

	printf("Done.\n");
	return 0;
}