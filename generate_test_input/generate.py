import math

off = 0xAA
blob_size = 61
byte_count = int(blob_size / math.gcd(blob_size, 8))

def make_word(byte_list_le):
	result = 0
	for i in range(0, math.ceil(blob_size/ 8)):
		result |= byte_list_le[i] << i * 8
	result &= (1 << blob_size) - 1
	return result

def shift_byte_list(byte_list_le):
	byte_off = int(blob_size / 8)
	bit_off = blob_size % 8
	for i in range(0, byte_count - byte_off - 1):
		byte_list_le[i] = (byte_list_le[byte_off + i] >> bit_off) | (byte_list_le[byte_off + i + 1] << (8 - bit_off))
	byte_list_le[byte_count - byte_off - 1] = byte_list_le[byte_count - 1] >> bit_off
	for i in range(byte_count - byte_off - 1, byte_count):
		byte_list_le[i] = 0
	return byte_list_le;

def make_byte_list():
	result = []
	for r in range(off, off + byte_count):
		result.append(r)
	return result;

byte_list = make_byte_list()
input_file = open("../sccompr/tests/testinput", "wb")
input_file.write(bytearray(byte_list))

print(f'const std::uint8_t input_buffer[{byte_count}] = {{')
for r in range(0, byte_count, 10):
	row = ""
	for c in range(r, min(byte_count, r + 10)):
		row += ' 0x{:02X}'.format(byte_list[c])
		if c != byte_count - 1:
			row += ','
	print(row)
print('};')

read_count = int(byte_count * 8 / blob_size)
bitset_vals = []
for i in range (0, read_count):
	bitset_vals.append(make_word(byte_list[0:blob_size]))
	byte_list = shift_byte_list(byte_list)
	
print(f'const std::uint64_t ctrl_bits[{read_count}] = {{')
for r in range(0, read_count, 4):
	row = ""
	for c in range(r, min(read_count, r + 4)):
		row += ' 0x{:016X}ull'.format(bitset_vals[c])
		if c != byte_count - 1:
			row += ','
	print(row)
print('};')
