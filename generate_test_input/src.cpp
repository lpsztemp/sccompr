#include <stdio.h>
#include <math.h>

int main(int argc, char** argv)
{
	FILE* data;
	constexpr size_t count = 1u << 20;
	double min = 1, max = 2;
	double delta = (max - min) / count;
	double val = min;
	data = fopen("../sccompr/tests/data8MiB", "wb");
	for (unsigned i = 0; i < count; ++i, val += delta)
		fwrite(&val, sizeof(double), 1, data);
	fclose(data);
	return 0;
}